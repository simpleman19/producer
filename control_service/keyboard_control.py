from time import sleep
from typing import Dict, Optional
from common.config import Config
from common.constants import SOCKET_SERVER_REFERENCE
from common.socket_messages import BaseMessage
from common.socket_server import SocketServerThread
from common.state_manager import StateManager
from common.thread import ProducerThread
from pynput.keyboard import Key, Controller
from pynput.mouse import Controller as MouseController, Button

from constants import KEY_TO_PYNPUT_COMPUTER_MAP


class KeyboardControlManager(ProducerThread):
    def __init__(self, sm: StateManager, config: Config, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sm = sm
        self.registered = False
        self.keyboard = Controller()
        self.mouse = MouseController()

    def run(self):
        print("Running keyboard control manager")
        while self.sm.running:
            if not self.registered:
                self.register_handlers()
            sleep(2)

    def register_handlers(self):
        socket_manager: SocketServerThread = self.sm.get_obj(SOCKET_SERVER_REFERENCE)
        if socket_manager:
            socket_manager.add_message_handler('SendString', self.handle_socket_message)
            socket_manager.add_message_handler('SendMouseClick', self.handle_socket_message_mouse_click)
            self.registered = True

    def handle_socket_message(self, msg: BaseMessage, *args, **kwargs):
        print(msg.data)
        string = msg.data.get('string', '')
        if KEY_TO_PYNPUT_COMPUTER_MAP.get(string, None):
            self.keyboard.type([KEY_TO_PYNPUT_COMPUTER_MAP.get(string, None)])
        else:
            self.keyboard.type(string)

    def handle_socket_message_mouse_click(self, msg: BaseMessage, *args, **kwargs):
        print(msg.data)
        button = msg.data.get('button', None)
        if button == '1':
            button = Button.left
        elif button == '3':
            button = Button.right
        else:
            button = None
        if button:
            self.mouse.click(button)
