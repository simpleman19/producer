from typing import List, Tuple, Type, Optional, Dict

from common.bootstrap_common import BootstrapApplication
from common.errors import merry
from common.thread import ProducerThread
from control_service.keyboard_control import KeyboardControlManager
from recording_server.config import load_config
from common.socket_server import SocketServerThread
from common.state_manager import StateManager

THREAD_CLASSES: List[Tuple[Type[ProducerThread], Optional[Dict]]] = [
    (SocketServerThread, None),
]


@merry._try
def run_control_service_app():
    state_manager = StateManager()
    config = load_config()

    dynamic_classes = [
        (KeyboardControlManager, None)
    ]

    bootstrap = BootstrapApplication(THREAD_CLASSES + dynamic_classes, state_manager, config)
    bootstrap.run()


if __name__ == '__main__':
    run_control_service_app()
