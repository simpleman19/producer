import tkinter as tk
import tk_tools
import socket
from typing import Optional
import os
from common.config import Config
from common.constants import INPUT_HANDLER_REFERENCE, MACHINE_MANAGER_REFERENCE
from common.thread import ProducerThread
from core.core_state_manager import CoreStateManager
from core.data import RemoteComputer
from core.input import InputHandlerThread
from core.machine_manager import MachineManagerThread, get_rtsp_record_video_url_for_machine

ip_addr = None


def get_ip():
    global ip_addr
    if not ip_addr:
        s = None
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(("8.8.8.8", 80))
            ip_addr = s.getsockname()[0]
        except:
            ip_addr = None
        finally:
            if s:
                s.close()
    return ip_addr


class ComputerFrame(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.name = tk.StringVar()
        self.name_frame = None
        self.connection = tk.StringVar()
        self.connection_frame = None
        self.video = tk.StringVar()
        self.video_frame = None
        self.recording = tk.StringVar()
        self.recording_frame = None
        self.separator = None

    def create_widgets(self):
        self.name_frame = tk.Label(self, textvariable=self.name)
        self.name_frame.pack()
        self.connection_frame = tk.Label(self, textvariable=self.connection)
        self.connection_frame.pack()
        self.video_frame = tk.Label(self, textvariable=self.video)
        self.video_frame.pack()
        self.recording_frame = tk.Label(self, textvariable=self.recording)
        self.recording_frame.pack()
        self.separator = tk.Frame(self, height=2, bd=1, relief=tk.SUNKEN)
        self.separator.pack(fill=tk.X, padx=5, pady=5)

    def update_fields(self, key=None, computer: RemoteComputer = None, mm: MachineManagerThread = None):
        self.name.set(f"Machine: {key} - {computer.name}")
        self.connection.set(f"{computer.control_connection_type}://{computer.control_ip}:{computer.control_port}")
        self.video.set(f"{get_rtsp_record_video_url_for_machine(computer)}")
        if not mm.status.get(key, None):
            self.recording.set("Recording: No")
        else:
            status = mm.status.get(key, {})
            path = status.get('path', None)
            if path:
                filename = os.path.basename(path)
            else:
                filename = "ERROR"
            size = status.get('file_size', -1)
            self.recording.set(f"Recording: Yes - {filename}:{size:.2f}MB")


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.hi_there = None
        self.quit = None
        self.led = None
        self.xbox_frame = None
        self.computers_frame = None
        self.output_window = None
        self.ip = tk.StringVar()
        self.ip_frame = None
        self.create_widgets()

    def create_widgets(self):
        self.xbox_frame = tk.Frame(self)
        self.xbox_frame.pack(side=tk.LEFT, fill=tk.Y)

        self.computers_frame = tk.Frame(self)
        self.computers_frame.pack(side=tk.LEFT, fill=tk.Y)

        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=self.master.destroy)
        self.quit.pack(side=tk.RIGHT)

        self.led = tk_tools.Led(self, size=50)
        self.led.to_green(False)
        self.led.pack(side=tk.RIGHT)

        self.output_window = tk.Text(self)
        self.output_window.pack(side=tk.LEFT)

        self.ip_frame = tk.Label(self, textvariable=self.ip)
        self.ip_frame.pack(side=tk.RIGHT)

    def create_xbox_child(self):
        fr = ComputerFrame(self.xbox_frame)
        fr.create_widgets()
        return fr

    def create_computer_child(self):
        fr = ComputerFrame(self.computers_frame)
        fr.create_widgets()
        return fr


class GuiThread(ProducerThread):
    def __init__(self, sm: CoreStateManager, config: Config, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.root: Optional[tk.Tk] = None
        self.app: Optional[Application] = None
        self.sm: CoreStateManager = sm
        self.config = config
        self.forwarding = False
        self.w, self.h = None, None
        self.xbox_frames = {}
        self.computer_frames = {}

    def run(self):
        self.init_gui()

    def heartbeat_100(self):
        self.update_status()

        self.update_computers()

        self.root.focus_force()

        if not self.sm.running and self.root:
            self.root.destroy()
        elif self.root:
            self.root.after(100, self.heartbeat_100)

    def heartbeat_2000(self):
        self.update_recording()

        ip = get_ip()
        if not ip:
            ip = 'loading...'
        self.app.ip.set('IP: ' + ip)

        if not self.sm.running and self.root:
            self.root.destroy()
        elif self.root:
            self.root.after(2000, self.heartbeat_2000)

    def init_gui(self):
        self.root = tk.Tk()
        if self.config.get("FULL_SCREEN", False):
            self.root.attributes('-fullscreen', True)
            self.w, self.h = self.root.winfo_screenwidth(), self.root.winfo_screenheight()
            self.root.geometry("%dx%d" % (self.w, self.h))
            self.root.config(cursor='none')
        else:
            self.w, self.h = self.root.winfo_screenwidth(), self.root.winfo_screenheight()
        self.app = Application(master=self.root)
        self.heartbeat_100()
        self.heartbeat_2000()
        self.app.mainloop()

    def update_status(self):
        input_obj: InputHandlerThread = self.sm.get_obj(INPUT_HANDLER_REFERENCE)
        if input_obj:
            self.update_from_input_obj(input_obj)

        while not self.sm._status_message_queue.empty():
            string = self.sm._status_message_queue.get()
            if self.app.output_window:
                self.app.output_window.insert(tk.END, string)
                self.app.output_window.see(tk.END)

    def update_from_input_obj(self, input_obj):
        override = input_obj.override_key_listener_func
        status = input_obj.macro_status()
        if override:
            self.app.led.to_grey(True)
        elif status == -1:
            self.app.led.to_red(True)
        elif status == 0:
            self.app.led.to_yellow(True)
        elif status == 1:
            self.app.led.to_green(True)
        if self.forwarding and not input_obj.forwarding:
            self.forwarding = input_obj.forwarding

    def update_computers(self):
        mm: Optional[MachineManagerThread] = self.sm.get_obj(MACHINE_MANAGER_REFERENCE)
        if mm:
            if mm.rtsp_process:
                self.root.withdraw()
            else:
                self.root.update()
                self.root.deiconify()
            for k, computer in mm.xboxes.items():
                if k not in self.xbox_frames.keys():
                    fr = self.app.create_xbox_child()
                    self.xbox_frames[k] = fr

                self.xbox_frames[k].update_fields(key=k, computer=computer, mm=mm)
            for k, computer in mm.computers.items():
                if k not in self.computer_frames.keys():
                    fr = self.app.create_computer_child()
                    self.computer_frames[k] = fr

                self.computer_frames[k].update_fields(key=k, computer=computer, mm=mm)

    def update_recording(self):
        mm: Optional[MachineManagerThread] = self.sm.get_obj(MACHINE_MANAGER_REFERENCE)
        if mm:
            mm.request_recording_status()


if __name__ == '__main__':
    root = tk.Tk()
    app = Application(master=root)
    app.mainloop()
