from common.bootstrap_common import BootstrapApplication
from common.errors import merry
from core.bootstrap_core import get_thread_class_list as get_core_class_list
from core.config import load_config
from core.core_state_manager import CoreStateManager
from core.input import InputHandlerThread
from gui.tkinter_main import GuiThread

THREAD_CLASSES = [
    (GuiThread, None),
    (InputHandlerThread, None)
]


@merry._try
def run_main_app():
    state_manager = CoreStateManager()
    config = load_config()  # TODO
    core_list = get_core_class_list(state_manager, config)
    bootstrap = BootstrapApplication(THREAD_CLASSES + core_list, state_manager, config)
    bootstrap.run()


if __name__ == '__main__':
    run_main_app()
