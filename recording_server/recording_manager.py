from time import sleep, time
from typing import Dict, Optional
import os
from shutil import copyfile
from common.config import Config
from common.constants import SOCKET_SERVER_REFERENCE
from common.socket_messages import BaseMessage, RecordingStatus
from common.socket_server import SocketServerThread
from common.state_manager import StateManager
from common.thread import ProducerThread
from common.video.ffmpeg import Ffmpeg, FfmpegTsRecorder
from common.file_utils import get_file_size, SIZE_UNIT, get_timestamp
from common.video.timestamp import timestamp_from_seconds


class RecordingManager(ProducerThread):
    def __init__(self, sm: StateManager, config: Config, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sm = sm
        self.recording_processes: Dict[str, Optional[FfmpegTsRecorder]] = {}
        self.registered = False
        self.recording_path = config.get('RECORDING_PATH', None)
        self.replay_path = config.get('REPLAY_PATH', None)

    def run(self):
        print("Running recording manager")
        while self.sm.running:
            if not self.registered:
                self.register_handlers()
            sleep(2)
        for url, proc in self.recording_processes.items():
            proc.stop()

    def register_handlers(self):
        socket_manager: SocketServerThread = self.sm.get_obj(SOCKET_SERVER_REFERENCE)
        if socket_manager:
            socket_manager.add_message_handler('RecorderManagement', self.handle_socket_message)
            socket_manager.add_message_handler('GetRecordingStatus', self.get_status)
            socket_manager.add_message_handler('RecorderCreateReplayFile', self.create_replay_file)
            self.registered = True

    def handle_socket_message(self, msg: BaseMessage, *args, **kwargs):
        print(f"Handling socket message: {msg.type_string}")
        action = msg.data.get('action', None)
        url = msg.data.get('url', None)
        output_name = msg.data.get('output_name', None)
        if action == 'start' and url:
            if not output_name:
                output_name = "unnamed"
            timestamp = get_timestamp()
            output_name = f"{output_name}_{timestamp}.mkv"
            path = os.path.join(self.recording_path, output_name)
            self.start_recording(url, path)
            print(f"Recording for {url} should be started and recording to {path}")
        elif action == 'stop' and url:
            self.stop_recording(url)
            print(f"Recording for {url} should be stopped")
        elif not url:
            print('Url not found')
        else:
            print("Unknown action")

    def get_status(self, *args, **kwargs):
        status = {}
        for k, v in self.recording_processes.items():
            if v:
                status[k] = {}
                status[k]['path'] = v.output_path
                status[k]['url'] = v.rtsp_url
                status[k]['file_size'] = get_file_size(v.output_path, SIZE_UNIT.MB)
        return RecordingStatus(recording_status=status)

    def start_recording(self, url, output_path):
        gstreamer = FfmpegTsRecorder()
        gstreamer.record_ts(ts_url=url, output_path=output_path)
        self.recording_processes[url] = gstreamer

    def stop_recording(self, url):
        rec = self.recording_processes.get(url, None)
        if rec:
            rec.stop()
        self.recording_processes[url] = None

    def create_replay_file(self, msg: BaseMessage, *args, **kwargs):
        ffmpeg = Ffmpeg()
        url = msg.data['url']
        length = msg.data['length']
        xbox_name = msg.data['xbox_name']
        gstreamer = self.recording_processes.get(url, None)
        if gstreamer:
            start_time = time() - gstreamer.time - length - 5
            if start_time < 0:
                start_time = 0
            start_timestamp = timestamp_from_seconds(start_time)
            length_timestamp = timestamp_from_seconds(length)
            output_path = os.path.join(self.replay_path, 'newest_replay.ts')
            ffmpeg.create_split_from_ts_file(gstreamer.output_path, start_timestamp, length_timestamp, output_path)
            if os.path.exists(output_path):
                save_path = os.path.join(self.replay_path, 'replays', f"{xbox_name}_{get_timestamp()}.mkv")
                copyfile(output_path, save_path)
        return self.get_status()
