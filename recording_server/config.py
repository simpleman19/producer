import os
from common.config import Config

basedir = os.path.abspath(os.path.dirname(__file__))


class DevelopmentConfig(Config):
    DEBUG = True
    SOCKET_COUNT = 5
    RTSP_PORT = 8550
    RECORDING_PATH = '/tank'
    REPLAY_PATH = '/tank/replays'


class ProductionConfig(Config):
    SOCKET_COUNT = 10
    RTSP_PORT = 8550


class TestingConfig(ProductionConfig):
    pass


def load_config(config_name=None):
    if not config_name:
        config_name = os.environ.get('APP_CONFIG', 'development')
    print("Starting up in: " + config_name)
    return config[config_name]()


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig
}
