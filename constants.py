from pynput.keyboard import Key

KEY_LEFT_CTRL = 0x80
KEY_LEFT_SHIFT = 0x81
KEY_LEFT_ALT = 0x82
KEY_LEFT_GUI = 0x83
KEY_RIGHT_CTRL = 0x84
KEY_RIGHT_SHIFT = 0x85
KEY_RIGHT_ALT = 0x86
KEY_RIGHT_GUI = 0x87
KEY_UP_ARROW = 0xDA
KEY_DOWN_ARROW = 0xD9
KEY_LEFT_ARROW = 0xD8
KEY_RIGHT_ARROW = 0xD7
KEY_BACKSPACE = 0xB2
KEY_TAB = 0xB3
KEY_RETURN = 0xB0
KEY_ESC = 0xB1
KEY_INSERT = 0xD1
KEY_DELETE = 0xD4
KEY_PAGE_UP = 0xD3
KEY_PAGE_DOWN = 0xD6
KEY_HOME = 0xD2
KEY_END = 0xD5
KEY_CAPS_LOCK = 0xC1
KEY_F1 = 0xC2
KEY_F2 = 0xC3
KEY_F3 = 0xC4
KEY_F4 = 0xC5
KEY_F5 = 0xC6
KEY_F6 = 0xC7
KEY_F7 = 0xC8
KEY_F8 = 0xC9
KEY_F9 = 0xCA
KEY_F10 = 0xCB
KEY_F11 = 0xCC
KEY_F12 = 0xCD
KEY_SPACE = 0x20

PYNPUT_XBOX_KEY_MAP = {
    Key.alt: KEY_LEFT_ALT,
    Key.alt_l: KEY_LEFT_ALT,
    Key.alt_r: KEY_RIGHT_ALT,
    Key.ctrl: KEY_LEFT_CTRL,
    Key.ctrl_l: KEY_LEFT_CTRL,
    Key.ctrl_r: KEY_RIGHT_CTRL,
    Key.backspace: KEY_BACKSPACE,
    Key.caps_lock: KEY_CAPS_LOCK,
    Key.esc: KEY_ESC,
    Key.space: KEY_SPACE,
    Key.up: KEY_UP_ARROW,
    Key.down: KEY_DOWN_ARROW,
    Key.left: KEY_LEFT_ARROW,
    Key.right: KEY_RIGHT_ARROW,
    Key.enter: 0x0A,
    Key.cmd: 0x83,
    Key.f1: 0xFE,
    Key.f2: 0xFD,
    Key.f3: 0xFC,
}

PYNPUT_COMPUTER_KEY_MAP = {
    Key.f1: 'f1',
    Key.f2: 'f2',
    Key.f3: 'f3',
    Key.f4: 'f4',
    Key.f5: 'f5',
    Key.f6: 'f6',
    Key.f7: 'f7',
    Key.f8: 'f8',
    Key.f9: 'f9',
    Key.f10: 'f10',
    Key.f11: 'f11',
    Key.f12: 'f12',
}

KEY_TO_PYNPUT_COMPUTER_MAP = {
    'f1': Key.f1,
    'f2': Key.f2,
    'f3': Key.f3,
    'f4': Key.f4,
    'f5': Key.f5,
    'f6': Key.f6,
    'f7': Key.f7,
    'f8': Key.f8,
    'f9': Key.f9,
    'f10': Key.f10,
    'f11': Key.f11,
    'f12': Key.f12,
}
