from typing import List, Tuple, Type, Optional, Dict

from common.bootstrap_common import BootstrapApplication
from common.errors import merry
from common.simple_rtsp_thread import RtspServerThread
from common.thread import ProducerThread
from common.video.stream_thread import StreamThread
from rpi.config import load_config
from common.socket_server import SocketServerThread
from common.state_manager import StateManager

THREAD_CLASSES: List[Tuple[Type[ProducerThread], Optional[Dict]]] = [
    (SocketServerThread, None),
]


@merry._try
def run_rpi_app():
    state_manager = StateManager()
    config = load_config()

    dynamic_classes = [
        (RtspServerThread, {'rtsp_port': config.get('RTSP_PORT')}),
        (StreamThread, None)
    ]

    bootstrap = BootstrapApplication(THREAD_CLASSES + dynamic_classes, state_manager, config)
    bootstrap.run()


if __name__ == '__main__':
    run_rpi_app()
