from smbus import SMBus

addr = 0x8  # bus address
bus = SMBus(1)  # indicates /dev/ic2-1
numb = 1


def writeData(value):
    byteValue = StringToBytes(value)
    bus.write_i2c_block_data(addr, 0x00, byteValue)  # first byte is 0=command byte.. just is.
    return -1


def StringToBytes(val):
    retVal = []
    for c in val:
        retVal.append(ord(c))
    return retVal


print("Enter 1 for ON or 0 for OFF")
while numb == 1:
    ledstate = input(">>>>   ")
    writeData(ledstate)
