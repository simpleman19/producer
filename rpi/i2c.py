from smbus import SMBus

from common.constants import I2C_MOUSE_MOVEMENT, int_to_byte
from common.logger import get_logger

logger = get_logger(__name__)

addr = 0x8  # bus address
numb = 1


def write_str_to_i2c(value):
    print("Sending: " + value)
    byte_value = string_to_bytes(value)
    write_block_data_to_i2c(byte_value)


def write_block_data_to_i2c(byte_val):
    bus = None
    try:
        bus = SMBus(1)
        bus.write_i2c_block_data(addr, 0x00, byte_val)
    except Exception as e:
        logger.error("I2C failed to initialize or send data")
        logger.error(e)
    finally:
        if bus:
            bus.close()


def string_to_bytes(val):
    ret_val = []
    for c in val:
        ret_val.append(ord(c))
    return ret_val


if __name__ == '__main__':
    # print("Enter 1 for ON or 0 for OFF")
    # print("Enter 1 for ON or 0 for OFF")
    # while numb == 1:
    #     ledstate = input(">>>>   ")
    #     write_str_to_i2c(ledstate)
    write_block_data_to_i2c([I2C_MOUSE_MOVEMENT, int_to_byte(100), int_to_byte(-100)])

