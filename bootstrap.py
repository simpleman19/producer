from typing import List
import multiprocessing
from multiprocessing.managers import Namespace
import signal
from time import sleep
from streambox.renderer import FrameRenderer
from streambox.processor import FrameProcessor
from streambox.loader import FrameLoader
from streambox.sb_thread import FrameThreadManager, SbThread
from streambox.server import SocketServerThread
from streambox.state_manager import StateManager, get_state_manager, MyProcessManager
from flask_bootstrap import boostrap_flask

threads: List[multiprocessing.Process] = []

THREADS_TO_RUN = [
    boostrap_flask,
]

THREAD_CLASSES = [
    SocketServerThread,
]

FRAME_THREAD_CLASSES = [
    FrameRenderer,
    FrameLoader,
    FrameProcessor
]

state_manager: StateManager


def startup(namespace: Namespace):
    global threads, state_manager
    for managed_thread in FRAME_THREAD_CLASSES:
        thread = FrameThreadManager(state_manager, managed_thread(state_manager))
        t = multiprocessing.Process(target=thread.run)
        t.daemon = True
        t.name = str(managed_thread)
        t.start()
        threads.append(t)
        del t

    for thread_class in THREAD_CLASSES:
        thread = thread_class(state_manager)
        t = multiprocessing.Process(target=thread.run)
        t.daemon = True
        t.name = str(thread)
        t.start()
        threads.append(t)
        del t

    for thread in THREADS_TO_RUN:
        t = multiprocessing.Process(target=thread, args=(state_manager, ))
        t.daemon = True
        t.name = str(thread)
        t.start()
        threads.append(t)
        del t


def handler(signum, frame):
    global state_manager
    print(f"Handling: {signum} by shutting down")
    state_manager.running = False


def init_sig_handlers():
    signals = [
        signal.SIGINT,
        signal.SIGTERM,
    ]
    for s in signals:
        signal.signal(s, handler)


if __name__ == '__main__':
    my_mgr = MyProcessManager()
    my_mgr.start()
    ns = my_mgr.Namespace()
    StateManager.init_namespace(ns)
    state_manager = my_mgr.StateManager(ns)

    init_sig_handlers()
    startup(ns)
    while state_manager.running:
        for t in threads:
            if not t.is_alive() and state_manager.running:
                print(f"Thread: {t.name} died")
        sleep(2)
    for t in threads:
        print(f"Waiting for: {t.name}")
        t.join()
