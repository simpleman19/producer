/*
  Arduino Slave for Raspberry Pi Master
  i2c_slave_ard.ino
  Connects to Raspberry Pi via I2C

  DroneBot Workshop 2019
  https://dronebotworkshop.com
*/

// Include the Wire library for I2C
#include <Wire.h>
#include <Keyboard.h>
#include <Mouse.h>

// LED on pin 13
const int ledPin = 17;

void setup() {
  Serial.begin(9600);

  // Join I2C bus as slave with address 8
  Wire.begin(0x8);

  // Call receiveEvent when data received
  Wire.onReceive(receiveEvent);

  // Setup pin 13 as output and turn LED off
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
}

// Function that executes whenever data is received from master
void receiveEvent(int howMany) {
  byte dataArray[howMany];

  for (int i = 0; i < howMany; i++) {
    dataArray[i] = Wire.read();
  }

  Serial.println("New message:");
  for (int i = 0; i < howMany; i++) {
    Serial.println(dataArray[i]);
  }

  if (howMany > 3 && dataArray[1] == 0x01) {
    Mouse.move(dataArray[2], dataArray[3], 0);
  } else if (howMany > 2 && dataArray[1] == 0x02) {
    if (dataArray[2] == 0x01) {
      Mouse.click(MOUSE_LEFT);
    } else if (dataArray[2] == 0x02) {
      Mouse.click(MOUSE_MIDDLE);
    } else if (dataArray[2] == 0x03) {
      Mouse.click(MOUSE_RIGHT);
    }
   } else {
    for (int i = 0; i < howMany; i++) {
      Keyboard.write(dataArray[i]);
    }
  }
}
void loop() {
  delay(100);
}
