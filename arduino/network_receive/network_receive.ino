#include <SPI.h>
#include <Ethernet.h>
#include <Keyboard.h>
#include <Mouse.h>

#define ETHCS 10 //W5500 CS
#define ETHRST 11 //W5500 RST
#define SDCS 4 //SD CS pin

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network.
// gateway and subnet are optional:
byte mac[] = {
  0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x07
};

// telnet defaults to port 23
EthernetServer server(23);
EthernetClient clients[8];

void setup() {
  pinMode(ETHCS, OUTPUT);
  pinMode(ETHRST, OUTPUT);
  pinMode(SDCS, OUTPUT);

  digitalWrite(ETHRST, HIGH);
  digitalWrite(ETHCS, HIGH);
  digitalWrite(SDCS, HIGH);

  Ethernet.init(ETHCS);

  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  //  while (!Serial) {
  //    ; // wait for serial port to connect. Needed for native USB port only
  //  }
  
  // start the Ethernet connection:
  Serial.println("Trying to get an IP address using DHCP");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // Check for Ethernet hardware present
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
      while (true) {
        delay(1); // do nothing, no point running without Ethernet hardware
      }
    }
    if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }
    // initialize the Ethernet device not using DHCP:
    Ethernet.begin(mac);
  }
  // print your local IP address:
  Serial.print("My IP address: ");
  Serial.println(Ethernet.localIP());
  
  // start listening for clients
  server.begin();
}

int iterCount = 0;

void loop() {
  if (iterCount % 20 == 0) {
    // check for any new client connecting, and say hello (before any incoming data)
    EthernetClient newClient = server.accept();
    if (newClient) {
      for (byte i = 0; i < 8; i++) {
        if (!clients[i]) {
          Serial.print("We have a new client #");
          Serial.println(i);
          newClient.print("ready");
          // Once we "accept", the client is no longer tracked by EthernetServer
          // so we must store it into our list of clients
          clients[i] = newClient;
          break;
        }
      }
    }
  }

  if (iterCount > 1000) {
    iterCount = 0;
  } else {
    iterCount += 1;
  }

  // check for incoming data from all clients
  for (byte i = 0; i < 8; i++) {
    if (clients[i] && clients[i].available() > 0) {
      // read bytes from a client
      byte buffer[80];
      int count = clients[i].read(buffer, 80);

      for (int j = 0; j < count; j++) {
        Serial.println(buffer[j]);
      }

      byte start = 0;
      while (start < count) {
        start = processMessage(buffer, start, count, 80, 0xAA) + 1;
      }
    }
  }

  // stop any clients which disconnect
  for (byte i = 0; i < 8; i++) {
    if (clients[i] && !clients[i].connected()) {
      Serial.print("disconnect client #");
      Serial.println(i);
      clients[i].stop();
    }
  }
  
  delay(5);
}

byte processMessage(byte ibuffer[], byte start, byte last, byte max_len, byte msg_end) {
  byte new_last = last;
  for (byte i = start; i < last; i++) {
    if (ibuffer[i] == msg_end) {
      new_last = i;
      Serial.println("Found end of message");
      break;
    }
  }

  byte count = new_last - start;
  Serial.println(count);
  if (count > 2 && ibuffer[start] == 0x01) {
    Mouse.move(ibuffer[start + 1], ibuffer[start + 2], 0);
  } else if (count > 1 && ibuffer[start] == 0x02) {
    if (ibuffer[start + 1] == 0x01) {
      Mouse.click(MOUSE_LEFT);
    } else if (ibuffer[start + 1] == 0x02) {
      Mouse.click(MOUSE_MIDDLE);
    } else if (ibuffer[start + 1] == 0x03) {
      Mouse.click(MOUSE_RIGHT);
    }
  } else if (count == 1 && ibuffer[start] == 0xFE) {
    Serial.println("F1");
    Keyboard.press(0x83);
    Keyboard.press('m');
    delay(100);
    Keyboard.releaseAll();
  } else if (count == 1 && ibuffer[start] == 0xFD) {
    Serial.println("F2");
    Keyboard.press(0x83);
    Keyboard.press('v');
    delay(100);
    Keyboard.releaseAll();
  } else if (count == 1 && ibuffer[start] == 0xFC) {
    Serial.println("F3");
    Keyboard.press(0x83);
    delay(4000);
    Keyboard.releaseAll();
  } else {
    for (byte i = start; i < new_last; i++) {
      Keyboard.write(ibuffer[i]);
    }
  }
  return new_last;
}
