from typing import TYPE_CHECKING

from pynput.keyboard import Key, KeyCode

from core.macros import KeyMatch

if TYPE_CHECKING:
    from core.machine_manager import MachineManagerThread
    from core.input import InputHandlerThread


def register_combos(input_obj: 'InputHandlerThread', manager: 'MachineManagerThread'):
    register_cod_2v2_combos(input_obj, manager)


def register_cod_2v2_combos(input_obj: 'InputHandlerThread', manager: 'MachineManagerThread'):
    PLAYER_1 = "1"
    PLAYER_2 = "2"
    PLAYER_3 = "6"
    PLAYER_4 = "7"

    def distribute_players_primary_full(*args, **kwargs):
        manager.send_xbox_key("1", PLAYER_1)
        manager.send_xbox_key("2", PLAYER_3)

    input_obj.register_macro(
        ([KeyMatch(Key.ctrl_r), KeyMatch(KeyCode(char='.'))], distribute_players_primary_full)
    )

    def distribute_players_primary_flipped(*args, **kwargs):
        manager.send_xbox_key("1", PLAYER_2)
        manager.send_xbox_key("2", PLAYER_4)

    input_obj.register_macro(
        ([KeyMatch(Key.ctrl_r), KeyMatch(KeyCode(char=','))], distribute_players_primary_flipped)
    )

    def distribute_players_another_first(*args, **kwargs):
        manager.send_xbox_key("3", PLAYER_1)
        manager.send_xbox_key("4", PLAYER_3)

    input_obj.register_macro(
        ([KeyMatch(Key.ctrl_r), KeyMatch(KeyCode(char='m'))], distribute_players_another_first)
    )

    def distribute_players_another_flipped(*args, **kwargs):
        manager.send_xbox_key("3", PLAYER_2)
        manager.send_xbox_key("4", PLAYER_4)

    input_obj.register_macro(
        ([KeyMatch(Key.ctrl_r), KeyMatch(KeyCode(char='n'))], distribute_players_another_flipped)
    )

    def distribute_players_secondary_first(*args, **kwargs):
        manager.send_xbox_key("5", PLAYER_1)
        manager.send_xbox_key("6", PLAYER_3)

    input_obj.register_macro(
        ([KeyMatch(Key.ctrl_r), KeyMatch(KeyCode(char=';'))], distribute_players_secondary_first)
    )

    def distribute_players_secondary_flipped(*args, **kwargs):
        manager.send_xbox_key("5", PLAYER_2)
        manager.send_xbox_key("6", PLAYER_4)

    input_obj.register_macro(
        ([KeyMatch(Key.ctrl_r), KeyMatch(KeyCode(char='l'))], distribute_players_secondary_flipped)
    )

    def distribute_players_tertiary_first(*args, **kwargs):
        manager.send_xbox_key("7", PLAYER_1)
        manager.send_xbox_key("8", PLAYER_3)

    input_obj.register_macro(
        ([KeyMatch(Key.ctrl_r), KeyMatch(KeyCode(char='p'))], distribute_players_tertiary_first)
    )

    def distribute_players_tertiary_flipped(*args, **kwargs):
        manager.send_xbox_key("7", PLAYER_2)
        manager.send_xbox_key("8", PLAYER_4)

    input_obj.register_macro(
        ([KeyMatch(Key.ctrl_r), KeyMatch(KeyCode(char='o'))], distribute_players_tertiary_flipped)
    )
