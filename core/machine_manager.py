import subprocess
from time import sleep
from typing import Dict, Optional

from pynput.keyboard import Key, KeyCode
from pynput.mouse import Button
import psutil

from common.config import Config
from common.constants import MACHINE_MANAGER_REFERENCE, INPUT_HANDLER_REFERENCE
from common.socket_messages import SendString, SendMouseMovement, SendMouseClick, RecorderManagement, \
    GetRecordingStatus, BaseMessage, RecorderCreateReplayFile
from common.thread import ProducerThread
from constants import PYNPUT_XBOX_KEY_MAP
from core.combo_keys import register_combos
from core.core_state_manager import CoreStateManager
from core.data import RemoteComputer
from core.input import InputHandlerThread
from core.macros import KeyMatch, Capture
from core.rtsp import show_rtsp_fullscreen_omx
from multiprocessing import Queue


def get_rtsp_audio_url_for_machine(machine: RemoteComputer):
    return f"rtsp://{machine.rtsp_ip}:{machine.rtsp_port}{machine.audio_endpoint}"


def get_rtsp_control_video_url_for_machine(machine: RemoteComputer):
    return f"rtsp://{machine.rtsp_ip}:{machine.rtsp_port}{machine.control_video_endpoint}"


def get_rtsp_record_video_url_for_machine(machine: RemoteComputer):
    return f"rtsp://{machine.rtsp_ip}:{machine.rtsp_port}{machine.record_vid_endpoint}"


def get_ts_record_video_url_for_machine(machine: RemoteComputer):
    return f"http://{machine.rtsp_ip}{machine.ts_video_endpoint}"


class MachineManagerThread(ProducerThread):
    def __init__(self, sm: CoreStateManager, config: Config, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sm = sm
        self.config = config
        self.get_rec_status = False
        self.xboxes: Dict[str, RemoteComputer] = config.get('XBOXES', {})
        self.computers: Dict[str, RemoteComputer] = config.get('COMPUTERS', {})
        self.recording_server: RemoteComputer = config.get('RECORDING_SERVER', None)
        self.forwarding_machine = None
        self.status = {}

        self.sm.register(MACHINE_MANAGER_REFERENCE, self)
        while not self.sm.get_obj(INPUT_HANDLER_REFERENCE):
            sleep(.5)
        self.input_obj: InputHandlerThread = self.sm.get_obj(INPUT_HANDLER_REFERENCE)
        if not self.input_obj:
            print("Error locating input object, could not register macro")
        else:
            self.input_obj.register_macro(
                ([KeyMatch(Key.alt), KeyMatch(KeyCode(char='c')), Capture()], self.toggle_forwarding)
            )
            self.input_obj.register_macro(
                ([KeyMatch(Key.alt), KeyMatch(KeyCode(char='x'))], self.toggle_forwarding)
            )
            self.input_obj.register_macro(
                ([KeyMatch(Key.alt), KeyMatch(KeyCode(char='k'))], self.toggle_rec_status)
            )
            self.input_obj.register_macro(
                ([KeyMatch(Key.alt), KeyMatch(KeyCode(char='r')), Capture()], self.toggle_recording_handler)
            )
            self.input_obj.register_macro(
                ([KeyMatch(Key.ctrl_r), KeyMatch(KeyCode(char='s')), Capture(), Capture()], self.send_xbox_key)
            )
            self.input_obj.register_macro(
                ([KeyMatch(Key.ctrl_r), KeyMatch(KeyCode(char='r')), Capture(), Capture()], self.make_replay)
            )
            register_combos(self.input_obj, self)

            self.input_obj.register_mouse_listener(self.mouse_forwarder)
        self.rtsp_process: Optional[subprocess.Popen] = None
        self.first_key = False
        self.key_queue: Queue = Queue()
        self.mouse_queue: Queue = Queue()
        self.last_key = None

    def run(self):
        while self.sm.running:
            self.process_key_queue()
            if not self.mouse_queue.empty():
                d_x, d_y, button = 0, 0, None
                while not self.mouse_queue.empty():
                    event_type, x, y, tmp_button = self.mouse_queue.get()
                    if event_type == "move":
                        d_x += x
                        d_y += y
                    elif event_type == "click":
                        button = tmp_button
                if d_x != 0 or d_y != 0:
                    msg = SendMouseMovement(x=d_x, y=d_y)
                    self.sm.send_socket_message(computer=self.forwarding_machine, message=msg)
                if button:
                    msg = SendMouseClick(button=button)
                    self.sm.send_socket_message(computer=self.forwarding_machine, message=msg)

            sleep(.05)

    def process_key_queue(self):
        if not self.key_queue.empty():
            if self.first_key:
                self.key_queue.get()
                self.first_key = False
                return

            str_to_send = b""
            while not self.key_queue.empty():
                key = self.key_queue.get(block=False, timeout=.1)
                if key:
                    if self.last_key == Key.alt_l and isinstance(key, KeyCode) and key.char == 'x':
                        self.toggle_forwarding()
                        return
                    self.last_key = key
                if isinstance(key, KeyCode):
                    str_to_send += key.char.encode('ascii')
                elif isinstance(key, Key) and key not in [Key.shift, Key.shift_l, Key.shift_r]:
                    temp = PYNPUT_XBOX_KEY_MAP.get(key, None)
                    if temp is not None:
                        str_to_send += bytes([temp])
                    else:
                        print("Unknown key: " + str(key))
            if str_to_send:
                send_string = SendString(str_val=str_to_send)
                self.sm.send_socket_message(computer=self.forwarding_machine, message=send_string)

    def toggle_forwarding(self, machine_number=None):
        if not self.rtsp_process and machine_number:
            print("Attempting to start forwarding")
            if machine_number in self.xboxes.keys():
                self.forwarding_machine: RemoteComputer = self.xboxes.get(machine_number, None)
                if self.forwarding_machine:
                    self.rtsp_process = show_rtsp_fullscreen_omx(
                        get_rtsp_control_video_url_for_machine(self.forwarding_machine))
                    self.first_key = True
                    self.input_obj.override_key_listener(self.key_forwarder)
                    msg = f"Starting forwarding on: {self.forwarding_machine.name}"
                else:
                    msg = f"Failed to find machine: {machine_number}"
            else:
                print("error, unknown machine number")
                msg = f"Unkown machine: {machine_number}"

        elif self.rtsp_process:
            print("Stopping forwarding")
            msg = f"Stopping forwarding on machine {self.forwarding_machine.name}"
            kill(self.rtsp_process.pid)
            self.rtsp_process = None
            self.forwarding_machine = None
            self.input_obj.clear_override_key_listener()
            print("Forwarding should be stopped")
        else:
            msg = "Unknown error in toggle forwarding handler"
        self.sm.print_status_message(msg)

    def toggle_rec_status(self):
        self.get_rec_status = not self.get_rec_status
        if self.get_rec_status:
            self.sm.print_status_message('Statusing Recordings Is Now On')
        else:
            self.sm.print_status_message('No Longer Statusing Recordings')

    def toggle_recording_handler(self, machine_number):
        if machine_number == 'a':
            all_recording = True
            for machine_num, _ in self.xboxes.items():
                if not self.status.get(machine_num, None):
                    all_recording = False
                    self.toggle_recording(machine_num)
            if all_recording:
                for machine_num, _ in self.xboxes.items():
                    self.toggle_recording(machine_num)
        else:
            if machine_number in self.xboxes:
                self.toggle_recording(machine_number)
            else:
                print(f"Unknown machine number: {machine_number}")

    def toggle_recording(self, machine_number):
        machine = self.xboxes[machine_number]
        if not self.status.get(machine_number, None):
            rec_msg = RecorderManagement(get_ts_record_video_url_for_machine(machine), 'start',
                                         f"output_{machine_number}")
            self.sm.send_socket_message(self.recording_server, rec_msg)
            msg = f"Starting recording on: {machine.name}"
        else:
            rec_msg = RecorderManagement(get_ts_record_video_url_for_machine(machine), 'stop')
            self.sm.send_socket_message(self.recording_server, rec_msg)
            msg = f"Stopping recording on: {machine.name}"
        self.sm.print_status_message(msg)
        sleep(.1)
        self.request_recording_status()

    def request_recording_status(self):
        if self.get_rec_status:
            status_msg = GetRecordingStatus()
            self.sm.send_socket_message(self.recording_server, status_msg, self.handle_status_message)

    def handle_status_message(self, resp: BaseMessage):
        status = resp.data.get('status', None)
        self.status = {}
        for num, computer in self.xboxes.items():
            url = get_ts_record_video_url_for_machine(computer)
            rec_status = status.get(url, None)
            self.status[num] = rec_status

    def send_xbox_key(self, machine_number, key):
        machine = self.xboxes.get(machine_number, None)
        if machine:
            str_to_send = key.encode('ascii')
            keyboard_msg = SendString(str_val=str_to_send)
            self.sm.send_socket_message(machine, keyboard_msg)
            self.sm.print_status_message(f"Sending key: {key} to xbox: {machine.name}")

    def send_computer_key(self, machine_number, key):
        machine = self.computers.get(machine_number, None)
        if machine:
            if isinstance(key, Button):
                click_message = SendMouseClick(button=key)
                self.sm.send_socket_message(machine, click_message)
                self.sm.print_status_message(f"Sending mouse button: {key} to computer: {machine.name}")
            else:
                str_to_send = key
                keyboard_msg = SendString(str_val=str_to_send)
                self.sm.send_socket_message(machine, keyboard_msg)
                self.sm.print_status_message(f"Sending key: {key} to computer: {machine.name}")

    def make_replay(self, machine_number, length):
        machine = self.xboxes.get(machine_number, None)
        if machine:
            create_replay = RecorderCreateReplayFile(
                url=get_ts_record_video_url_for_machine(machine),
                length=int(length) * 10,
                xbox_name=machine.name
            )
            self.sm.send_socket_message(self.recording_server, create_replay, self.replay_finished)
            self.sm.print_status_message(f"Creating replay of xbox: {machine.name} of last {int(length) * 10} seconds")

    def replay_finished(self, *args, **kwargs):
        self.sm.print_status_message("Finished creating replay...")

    # TODO definitely not fast enough, looks like some sort of buffer issue on arduino
    def key_forwarder(self, new_key: Key):
        if self.forwarding_machine:
            self.key_queue.put(new_key)

    def mouse_forwarder(self, event_type=None, d_x=None, d_y=None, button=None):
        if event_type and self.forwarding_machine:
            if d_x != 0 or d_y != 0 or button:
                self.mouse_queue.put((event_type, d_x, d_y, button))

    def add_machine(self, ip, name):
        if ip:
            self.xboxes[ip] = RemoteComputer(control_ip=ip, name=name)
            self._generate_machine_config(ip)

    def get_machine_config(self, ip):
        pass

    def _generate_machine_config(self, ip):
        machine: RemoteComputer = self.xboxes[ip]
        if machine:
            machine.control_port = 5001
            machine.rtsp_port = 8550
            machine.control_video_endpoint = "/live"
            machine.audio_endpoint = "/livea"
            self.xboxes[ip] = machine


def kill(proc_pid):
    process = psutil.Process(proc_pid)
    for proc in process.children(recursive=True):
        proc.kill()
    process.kill()
