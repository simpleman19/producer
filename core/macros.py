import timeit
from typing import List, Callable, Tuple
from pynput.keyboard import Key, KeyCode
import pynput

from core.core_state_manager import CoreStateManager


class MacroKey(object):
    def __init__(self, val=None):
        self.val = val

    def matches(self, val):
        return val == self.val


class KeyMatch(MacroKey):
    pass


class Capture(MacroKey):
    def matches(self, val):
        return True


class Matcher(object):
    def __init__(self, sm: CoreStateManager):
        self.macro_list: List[Tuple[List, Callable]] = []
        self.current_key_list = []
        self.current_match_list = self.macro_list
        self.match = None
        self.failed = False
        self.start_time = timeit.default_timer()
        self.sm: CoreStateManager = sm

    def is_key_list_empty(self):
        return len(self.current_key_list) == 0

    def is_matching(self):
        return not self.is_key_list_empty() and not self.failed_match()

    def failed_match(self):
        return self.failed

    def reset_matcher(self):
        self.current_key_list = []
        self.current_match_list = self.macro_list
        self.match = None
        self.failed = False
        self.start_time = timeit.default_timer()

    def add_key(self, new_key):
        if self.is_key_list_empty() or timeit.default_timer() - self.start_time > 4:
            self.reset_matcher()

        if new_key:
            ret_val = self._add_key(new_key)
            if ret_val is True:
                self.execute_match()
            elif ret_val is None:
                print("Still looking for matches")
            return ret_val
        return None

    def _add_key(self, new_key):
        self.current_key_list.append(new_key)

        for i in range(len(self.current_key_list)):
            new_matches = []
            for match_tuple in self.current_match_list:
                match, _ = match_tuple
                is_match = True
                for j in range(i + 1):
                    if not match[j].matches(self.current_key_list[j]):
                        is_match = False
                        break
                if is_match:
                    new_matches.append(match_tuple)
            self.current_match_list = new_matches
        if len(self.current_match_list) == 1 and len(self.current_match_list[0][0]) == len(self.current_key_list):
            self.match = self.current_match_list[0]
            self.failed = False
            return True
        elif len(self.current_match_list) >= 1:
            self.failed = False
            return None
        self.failed = True
        return False

    def execute_match(self):
        if not self.match:
            return False
        match_keys_objs, callback = self.match
        _args = []
        pretty_string = ""
        for i, key_obj in enumerate(match_keys_objs):
            if isinstance(key_obj, Capture) and isinstance(self.current_key_list[i], KeyCode):
                _args.append(self.current_key_list[i].char)
            pretty_string += str(self.current_key_list[i]) + ", "
        pretty_string = pretty_string[:-2]
        self.sm.print_status_message(f"Executing Macro: [{pretty_string}]")
        ret_val = callback(*_args)
        self.reset_matcher()
        return ret_val

    def register_macro(self, macro_tuple: Tuple[List, Callable]):
        self.macro_list.append(macro_tuple)


if __name__ == '__main__':
    def print_key(key=None):
        print(f"Ctrl + x + key: {key}")


    def print_key2(key=None):
        print(f"Alt + x + key: {key}")


    key_list = [
        ([KeyMatch(Key.ctrl), KeyMatch(KeyCode(char='x')), Capture()], print_key),
        ([KeyMatch(Key.alt), KeyMatch(KeyCode(char='x')), Capture()], print_key2)
    ]
    m = Matcher(key_list)


    def add_key(key):
        ret_val = m.add_key(key)
        if ret_val == True:
            m.execute_match()
        elif ret_val == None:
            print("Still looking for matches")
        elif ret_val == False:
            m.reset_matcher()
        return True


    with pynput.keyboard.Listener(on_press=add_key) as listener:
        listener.join()
