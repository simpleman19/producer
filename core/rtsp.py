import shutil
import subprocess
from threading import Thread
from time import sleep
from typing import Optional

thread: Optional[Thread] = None
popen: Optional[subprocess.Popen] = None


def launch_thread_for_rtsp():
    global thread
    t = Thread(target=show_rtsp_fullscreen)
    t.daemon = True
    t.name = str(thread)
    t.start()
    thread = t
    del t


def kill_thread():
    global popen
    popen.kill()


def show_rtsp_fullscreen(url="rtsp://10.0.0.33:8554/0") -> subprocess.Popen:
    ffplay = shutil.which("ffplay")
    if not ffplay:
        raise FileNotFoundError("Failed to find ffplay")
    # cmd = [gst_launch, "rtspsrc", "location=" + url, "latency=250", "buffer-mode=auto", "!", "decodebin", "!",
    #        "queue", "leaky=2", "!", "videoconvert", "!", "fbdevsink"]
    cmd = [ffplay, '-fflags', 'nobuffer', '-fs', '-framedrop', '-rtsp_transport', 'tcp', url]
    return subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)


def show_rtsp_fullscreen_omx(url="rtsp://10.0.0.33:8554/0") -> subprocess.Popen:
    omxplayer = shutil.which("omxplayer")
    if not omxplayer:
        raise FileNotFoundError("Failed to find omxplayer")
    # player="omxplayer --no-keys --no-osd --avdict rtsp_transport:tcp --win \"${window_positions[$x]}\" \"${
    # camera_feeds[$i]}\" --live -n -1 --timeout "$omx_timeout" --dbus_name "org.mpris.MediaPlayer2.omxplayer.${
    # camera_names[$i]}" >/dev/null &"
    cmd = [omxplayer, '--no-keys', '--no-osd', '--avdict', 'rtsp_transport:tcp', '--live', '--threshold', '.05', url]
    return subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)


def show_rtsp_fullscreen_gst(url="rtsp://10.0.0.33:8554/0") -> subprocess.Popen:
    print("Launching gstreamer")
    gst_launch = shutil.which("gst-launch-1.0")
    if not gst_launch:
        raise FileNotFoundError("Failed to find gst-launch-1.0")
    cmd = [gst_launch, "rtspsrc", "location=" + url, "latency=250", "buffer-mode=auto", "!", "decodebin", "!",
           "queue", "leaky=2", "!", "videoconvert", "!", "autovideosink"]
    return subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)


if __name__ == "__main__":
    launch_thread_for_rtsp()
    sleep(20)
    kill_thread()
    thread.join()
