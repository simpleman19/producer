from pynput.mouse import Button

PLAYER_1 = "1"
PLAYER_2 = "6"

COD_1_V_1 = {
    '1': {
        PLAYER_1: ('1', PLAYER_1),
        PLAYER_2: ('2', PLAYER_2),
        'd': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_2: ('2', PLAYER_2),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_2: ('2', PLAYER_2),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_2: ('4', PLAYER_2),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_2: ('6', PLAYER_2),
        }
    },
    '2': {
        PLAYER_1: ('3', PLAYER_1),
        PLAYER_2: ('4', PLAYER_2),
        'd': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_2: ('4', PLAYER_2),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_2: ('2', PLAYER_2),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_2: ('4', PLAYER_2),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_2: ('6', PLAYER_2),
        }
    },
    '3': {
        PLAYER_1: ('5', PLAYER_1),
        PLAYER_2: ('6', PLAYER_2),
        'd': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_2: ('6', PLAYER_2),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_2: ('2', PLAYER_2),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_2: ('4', PLAYER_2),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_2: ('6', PLAYER_2),
        }
    }
}

PLAYER_1 = "1"
PLAYER_2 = "2"
PLAYER_3 = "6"
PLAYER_4 = "7"

COD_2_V_2 = {
    '1': {
        PLAYER_1: ('1', PLAYER_1),
        PLAYER_2: ('1', PLAYER_2),
        PLAYER_3: ('2', PLAYER_3),
        PLAYER_4: ('2', PLAYER_4),
        'd': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_2: ('2', PLAYER_3),
        },
        'f': {
            PLAYER_1: ('1', PLAYER_2),
            PLAYER_2: ('2', PLAYER_4),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_2: ('2', PLAYER_3),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_2: ('4', PLAYER_3),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_2: ('6', PLAYER_3),
        }
    },
    '2': {
        PLAYER_1: ('3', PLAYER_1),
        PLAYER_2: ('3', PLAYER_2),
        PLAYER_3: ('4', PLAYER_3),
        PLAYER_4: ('4', PLAYER_4),
        'd': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_2: ('4', PLAYER_3),
        },
        'f': {
            PLAYER_1: ('3', PLAYER_2),
            PLAYER_2: ('4', PLAYER_4),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_2: ('2', PLAYER_3),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_2: ('4', PLAYER_3),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_2: ('6', PLAYER_3),
        }
    },
    '3': {
        PLAYER_1: ('5', PLAYER_1),
        PLAYER_2: ('5', PLAYER_2),
        PLAYER_3: ('6', PLAYER_3),
        PLAYER_4: ('6', PLAYER_4),
        'd': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_2: ('6', PLAYER_3),
        },
        'f': {
            PLAYER_1: ('5', PLAYER_2),
            PLAYER_2: ('6', PLAYER_4),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_2: ('2', PLAYER_3),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_2: ('4', PLAYER_3),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_2: ('6', PLAYER_3),
        }
    }
}

PLAYER_1 = "1"
PLAYER_2 = "2"
PLAYER_3 = "3"
PLAYER_4 = "6"
PLAYER_5 = "7"
PLAYER_6 = "8"

COD_3_V_3 = {
    '1': {
        PLAYER_1: ('1', PLAYER_1),
        PLAYER_2: ('1', PLAYER_2),
        PLAYER_3: ('2', PLAYER_3),
        PLAYER_4: ('2', PLAYER_4),
        PLAYER_5: ('3', PLAYER_5),
        PLAYER_6: ('3', PLAYER_6),
        'd': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_2: ('2', PLAYER_3),
            PLAYER_3: ('3', PLAYER_5),
        },
        'f': {
            PLAYER_1: ('1', PLAYER_2),
            PLAYER_2: ('2', PLAYER_4),
            PLAYER_3: ('3', PLAYER_6),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_2: ('2', PLAYER_3),
            PLAYER_3: ('3', PLAYER_5),
        },
        'k': {
            PLAYER_1: ('4', PLAYER_1),
            PLAYER_3: ('5', PLAYER_3),
            PLAYER_5: ('6', PLAYER_5),
        },
    },
    '2': {
        PLAYER_1: ('3', PLAYER_1),
        PLAYER_2: ('3', PLAYER_2),
        PLAYER_3: ('4', PLAYER_3),
        PLAYER_4: ('4', PLAYER_4),
        PLAYER_5: ('5', PLAYER_5),
        PLAYER_6: ('5', PLAYER_6),
        'd': {
            PLAYER_1: ('4', PLAYER_1),
            PLAYER_3: ('5', PLAYER_3),
            PLAYER_5: ('6', PLAYER_5),
        },
        'f': {
            PLAYER_1: ('4', PLAYER_1),
            PLAYER_3: ('5', PLAYER_3),
            PLAYER_5: ('6', PLAYER_5),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_2: ('2', PLAYER_3),
            PLAYER_3: ('3', PLAYER_5),
        },
        'k': {
            PLAYER_1: ('4', PLAYER_1),
            PLAYER_3: ('5', PLAYER_3),
            PLAYER_5: ('6', PLAYER_5),
        },
    },
}

PLAYER_1 = "1"
PLAYER_2 = "2"
PLAYER_3 = "3"
PLAYER_4 = "4"
PLAYER_5 = "5"
PLAYER_6 = "6"
PLAYER_7 = "7"
PLAYER_8 = "8"

NEW_COD_3_V_3 = {
    '1': {
        'name': 'COD 3v3 Config 1',
        PLAYER_1: ('1', PLAYER_1),
        PLAYER_2: ('1', PLAYER_2),
        PLAYER_3: ('1', PLAYER_3),
        PLAYER_5: ('2', PLAYER_5),
        PLAYER_6: ('2', PLAYER_6),
        PLAYER_7: ('2', PLAYER_7),
        'd': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_5: ('2', PLAYER_5),
        },
        'f': {
            PLAYER_2: ('1', PLAYER_2),
            PLAYER_6: ('2', PLAYER_6),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_5: ('2', PLAYER_5),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_5: ('4', PLAYER_5),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_5: ('6', PLAYER_5),
        },
    },
    '2': {
        'name': 'COD 3v3 Config 2',
        PLAYER_1: ('3', PLAYER_1),
        PLAYER_2: ('3', PLAYER_2),
        PLAYER_3: ('3', PLAYER_3),
        PLAYER_5: ('4', PLAYER_5),
        PLAYER_6: ('4', PLAYER_6),
        PLAYER_7: ('4', PLAYER_7),
        'd': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_5: ('4', PLAYER_5),
        },
        'f': {
            PLAYER_2: ('3', PLAYER_2),
            PLAYER_6: ('4', PLAYER_6),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_5: ('2', PLAYER_5),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_5: ('4', PLAYER_5),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_5: ('6', PLAYER_5),
        },
    },
    '3': {
        'name': 'COD 3v3 Config 3',
        PLAYER_1: ('5', PLAYER_1),
        PLAYER_2: ('5', PLAYER_2),
        PLAYER_3: ('5', PLAYER_3),
        PLAYER_5: ('6', PLAYER_5),
        PLAYER_6: ('6', PLAYER_6),
        PLAYER_7: ('6', PLAYER_7),
        'd': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_5: ('6', PLAYER_5),
        },
        'f': {
            PLAYER_2: ('5', PLAYER_2),
            PLAYER_6: ('6', PLAYER_6),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_5: ('2', PLAYER_5),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_5: ('4', PLAYER_5),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_5: ('6', PLAYER_5),
        },
    },
}

NEW_COD_4_V_4 = {
    '1': {
        'name': 'COD 4v4 Config 1',
        PLAYER_1: ('1', PLAYER_1),
        PLAYER_2: ('1', PLAYER_2),
        PLAYER_3: ('1', PLAYER_3),
        PLAYER_4: ('1', PLAYER_4),
        PLAYER_5: ('2', PLAYER_5),
        PLAYER_6: ('2', PLAYER_6),
        PLAYER_7: ('2', PLAYER_7),
        PLAYER_8: ('2', PLAYER_8),
        'd': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_5: ('2', PLAYER_5),
        },
        'f': {
            PLAYER_2: ('1', PLAYER_2),
            PLAYER_6: ('2', PLAYER_6),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_5: ('2', PLAYER_5),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_5: ('4', PLAYER_5),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_5: ('6', PLAYER_5),
        },
        ';': {
            PLAYER_1: ('7', PLAYER_1),
            PLAYER_5: ('8', PLAYER_5),
        },
    },
    '2': {
        'name': 'COD 4v4 Config 2',
        PLAYER_1: ('3', PLAYER_1),
        PLAYER_2: ('3', PLAYER_2),
        PLAYER_3: ('3', PLAYER_3),
        PLAYER_4: ('3', PLAYER_4),
        PLAYER_5: ('4', PLAYER_5),
        PLAYER_6: ('4', PLAYER_6),
        PLAYER_7: ('4', PLAYER_7),
        PLAYER_8: ('4', PLAYER_8),
        'd': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_5: ('4', PLAYER_5),
        },
        'f': {
            PLAYER_2: ('3', PLAYER_2),
            PLAYER_6: ('4', PLAYER_6),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_5: ('2', PLAYER_5),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_5: ('4', PLAYER_5),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_5: ('6', PLAYER_5),
        },
        ';': {
            PLAYER_1: ('7', PLAYER_1),
            PLAYER_5: ('8', PLAYER_5),
        },
    },
    '3': {
        'name': 'COD 4v4 Config 3',
        PLAYER_1: ('5', PLAYER_1),
        PLAYER_2: ('5', PLAYER_2),
        PLAYER_3: ('5', PLAYER_3),
        PLAYER_4: ('5', PLAYER_4),
        PLAYER_5: ('6', PLAYER_5),
        PLAYER_6: ('6', PLAYER_6),
        PLAYER_7: ('6', PLAYER_7),
        PLAYER_8: ('6', PLAYER_8),
        'd': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_5: ('6', PLAYER_5),
        },
        'f': {
            PLAYER_2: ('5', PLAYER_2),
            PLAYER_6: ('6', PLAYER_6),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_5: ('2', PLAYER_5),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_5: ('4', PLAYER_5),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_5: ('6', PLAYER_5),
        },
        ';': {
            PLAYER_1: ('7', PLAYER_1),
            PLAYER_5: ('8', PLAYER_5),
        },
    },
    '4': {
        'name': 'COD 4v4 Config 4',
        PLAYER_1: ('7', PLAYER_1),
        PLAYER_2: ('7', PLAYER_2),
        PLAYER_3: ('7', PLAYER_3),
        PLAYER_4: ('7', PLAYER_4),
        PLAYER_5: ('8', PLAYER_5),
        PLAYER_6: ('8', PLAYER_6),
        PLAYER_7: ('8', PLAYER_7),
        PLAYER_8: ('8', PLAYER_8),
        'd': {
            PLAYER_1: ('7', PLAYER_1),
            PLAYER_5: ('8', PLAYER_5),
        },
        'f': {
            PLAYER_2: ('7', PLAYER_2),
            PLAYER_6: ('8', PLAYER_6),
        },
        'j': {
            PLAYER_1: ('1', PLAYER_1),
            PLAYER_5: ('2', PLAYER_5),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            PLAYER_5: ('4', PLAYER_5),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            PLAYER_5: ('6', PLAYER_5),
        },
        ';': {
            PLAYER_1: ('7', PLAYER_1),
            PLAYER_5: ('8', PLAYER_5),
        },
    },
}

ROGUE_COMPANY_CONFIG = {
    '1': {
        'name': 'Rogue Company Config 1',
        'q': ('1', Button.left),
        'w': ('1', Button.right),
        'e': ('2', Button.left),
        'r': ('2', Button.right),
        'a': ('3', Button.left),
        's': ('3', Button.right),
        'd': ('4', Button.left),
        'f': ('4', Button.right),
        'z': ('5', Button.left),
        'x': ('5', Button.right),
        'c': ('6', Button.left),
        'v': ('6', Button.right),
    },
}
