PLAYER_1 = "1"
PLAYER_2 = "2"
PLAYER_3 = "3"
PLAYER_4 = "4"
PLAYER_5 = "5"
PLAYER_6 = "6"
PLAYER_7 = "7"
PLAYER_8 = "8"
PLAYER_9 = "9"
PLAYER_10 = "0"

DEFAULT = "1"

VALORANT = {
    '1': {
        'name': 'Valorant Config 1',
        PLAYER_1: ("1", PLAYER_1),
        PLAYER_2: ("1", PLAYER_2),
        PLAYER_3: ("1", PLAYER_3),
        PLAYER_4: ("1", PLAYER_4),
        PLAYER_5: ("1", PLAYER_5),
        PLAYER_6: ("1", PLAYER_6),
        PLAYER_7: ("1", PLAYER_7),
        PLAYER_8: ("1", PLAYER_8),
        PLAYER_9: ("1", PLAYER_9),
        PLAYER_10: ("1", PLAYER_10),
        "f1": ("1", "v"),
        "f2": ("2", "v"),
        "f3": ("3", "v"),
        "f4": ("4", "v"),
        "f5": ("5", "v"),
        "f6": ("6", "v"),
        'd': {
            PLAYER_7: ('1', PLAYER_1),
            DEFAULT: ('2', DEFAULT),
        },
        'j': {
            PLAYER_7: ('1', PLAYER_1),
            DEFAULT: ('2', DEFAULT),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            DEFAULT: ('4', DEFAULT),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            DEFAULT: ('6', DEFAULT),
        }
    },
    '2': {
        'name': 'Valorant Config 2',
        PLAYER_1: ("3", PLAYER_1),
        PLAYER_2: ("3", PLAYER_2),
        PLAYER_3: ("3", PLAYER_3),
        PLAYER_4: ("3", PLAYER_4),
        PLAYER_5: ("3", PLAYER_5),
        PLAYER_6: ("3", PLAYER_6),
        PLAYER_7: ("3", PLAYER_7),
        PLAYER_8: ("3", PLAYER_8),
        PLAYER_9: ("3", PLAYER_9),
        PLAYER_10: ("3", PLAYER_10),
        "f1": ("1", "v"),
        "f2": ("2", "v"),
        "f3": ("3", "v"),
        "f4": ("4", "v"),
        "f5": ("5", "v"),
        "f6": ("6", "v"),
        'd': {
            PLAYER_7: ('3', PLAYER_1),
            DEFAULT: ('4', DEFAULT),
        },
        'j': {
            PLAYER_7: ('1', PLAYER_1),
            DEFAULT: ('2', DEFAULT),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            DEFAULT: ('4', DEFAULT),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            DEFAULT: ('6', DEFAULT),
        }
    },
    '3': {
        'name': 'Valorant Config 3',
        PLAYER_1: ("5", PLAYER_1),
        PLAYER_2: ("5", PLAYER_2),
        PLAYER_3: ("5", PLAYER_3),
        PLAYER_4: ("5", PLAYER_4),
        PLAYER_5: ("5", PLAYER_5),
        PLAYER_6: ("5", PLAYER_6),
        PLAYER_7: ("5", PLAYER_7),
        PLAYER_8: ("5", PLAYER_8),
        PLAYER_9: ("5", PLAYER_9),
        PLAYER_10: ("5", PLAYER_10),
        "f1": ("1", "v"),
        "f2": ("2", "v"),
        "f3": ("3", "v"),
        "f4": ("4", "v"),
        "f5": ("5", "v"),
        "f6": ("6", "v"),
        'd': {
            PLAYER_7: ('5', PLAYER_1),
            DEFAULT: ('6', DEFAULT),
        },
        'j': {
            PLAYER_7: ('1', PLAYER_1),
            DEFAULT: ('2', DEFAULT),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            DEFAULT: ('4', DEFAULT),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            DEFAULT: ('6', DEFAULT),
        }
    },
}
