PLAYER_1 = "f1"
PLAYER_2 = "f2"
PLAYER_3 = "f3"
PLAYER_4 = "f4"
PLAYER_5 = "f5"
PLAYER_6 = "f6"
PLAYER_7 = "f7"
PLAYER_8 = "f8"
PLAYER_9 = "f9"
PLAYER_10 = "f10"
PLAYER_11 = "f11"
PLAYER_12 = "f12"

DEFAULT = "f1"

OVERWATCH = {
    '1': {
        'name': 'Overwatch Config 1',
        PLAYER_1: ("1", PLAYER_1),
        PLAYER_2: ("1", PLAYER_2),
        PLAYER_3: ("1", PLAYER_3),
        PLAYER_4: ("1", PLAYER_4),
        PLAYER_5: ("1", PLAYER_5),
        PLAYER_6: ("1", PLAYER_6),
        PLAYER_7: ("1", PLAYER_7),
        PLAYER_8: ("1", PLAYER_8),
        PLAYER_9: ("1", PLAYER_9),
        PLAYER_10: ("1", PLAYER_10),
        PLAYER_11: ("1", PLAYER_11),
        PLAYER_12: ("1", PLAYER_12),
        "1": ("1", " "),
        "2": ("2", " "),
        "3": ("3", " "),
        "4": ("4", " "),
        "5": ("5", " "),
        "6": ("6", " "),
        'd': {
            PLAYER_7: ('1', PLAYER_1),
            DEFAULT: ('2', DEFAULT),
        },
        'j': {
            PLAYER_7: ('1', PLAYER_1),
            DEFAULT: ('2', DEFAULT),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            DEFAULT: ('4', DEFAULT),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            DEFAULT: ('6', DEFAULT),
        }
    },
    '2': {
        'name': 'Overwatch Config 2',
        PLAYER_1: ("3", PLAYER_1),
        PLAYER_2: ("3", PLAYER_2),
        PLAYER_3: ("3", PLAYER_3),
        PLAYER_4: ("3", PLAYER_4),
        PLAYER_5: ("3", PLAYER_5),
        PLAYER_6: ("3", PLAYER_6),
        PLAYER_7: ("3", PLAYER_7),
        PLAYER_8: ("3", PLAYER_8),
        PLAYER_9: ("3", PLAYER_9),
        PLAYER_10: ("3", PLAYER_10),
        PLAYER_11: ("3", PLAYER_11),
        PLAYER_12: ("3", PLAYER_12),
        "1": ("1", " "),
        "2": ("2", " "),
        "3": ("3", " "),
        "4": ("4", " "),
        "5": ("5", " "),
        "6": ("6", " "),
        'd': {
            PLAYER_7: ('1', PLAYER_1),
            DEFAULT: ('2', DEFAULT),
        },
        'j': {
            PLAYER_7: ('1', PLAYER_1),
            DEFAULT: ('2', DEFAULT),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            DEFAULT: ('4', DEFAULT),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            DEFAULT: ('6', DEFAULT),
        }
    },
    '3': {
        'name': 'Overwatch Config 3',
        PLAYER_1: ("5", PLAYER_1),
        PLAYER_2: ("5", PLAYER_2),
        PLAYER_3: ("5", PLAYER_3),
        PLAYER_4: ("5", PLAYER_4),
        PLAYER_5: ("5", PLAYER_5),
        PLAYER_6: ("5", PLAYER_6),
        PLAYER_7: ("5", PLAYER_7),
        PLAYER_8: ("5", PLAYER_8),
        PLAYER_9: ("5", PLAYER_9),
        PLAYER_10: ("5", PLAYER_10),
        PLAYER_11: ("5", PLAYER_11),
        PLAYER_12: ("5", PLAYER_12),
        "1": ("1", " "),
        "2": ("2", " "),
        "3": ("3", " "),
        "4": ("4", " "),
        "5": ("5", " "),
        "6": ("6", " "),
        'd': {
            PLAYER_7: ('1', PLAYER_1),
            DEFAULT: ('2', DEFAULT),
        },
        'j': {
            PLAYER_7: ('1', PLAYER_1),
            DEFAULT: ('2', DEFAULT),
        },
        'k': {
            PLAYER_1: ('3', PLAYER_1),
            DEFAULT: ('4', DEFAULT),
        },
        'l': {
            PLAYER_1: ('5', PLAYER_1),
            DEFAULT: ('6', DEFAULT),
        }
    },
}
