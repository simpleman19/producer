from common.bootstrap_common import BootstrapApplication
from common.errors import merry
from core.config import load_config
from core.core_state_manager import CoreStateManager
from core.machine_manager import MachineManagerThread
from core.new_xbox_macros import NewXboxMacros
from core.socket_manager import SocketManager

THREAD_CLASSES = [
    (SocketManager, None),
    (MachineManagerThread, None),
    (NewXboxMacros, None),
]


@merry._try
def run_core_app():
    state_manager = CoreStateManager()
    config = load_config()
    bootstrap = BootstrapApplication(THREAD_CLASSES, state_manager, config)
    bootstrap.run()


def get_thread_class_list(state_manager: CoreStateManager, config):
    return [
        (BootstrapApplication, {'thread_classes': THREAD_CLASSES, 'sm': state_manager, 'config': config})
    ]


if __name__ == '__main__':
    run_core_app()
