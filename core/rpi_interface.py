import socket
from common.constants import SOCKET_PORT
from common.socket_messages import encode, decode, BaseMessage, SendMouseMovement
from time import sleep

HOST = '10.0.0.185'  # The remote host


class RpiInterface(object):
    def __init__(self):
        self.s = None

    def _send_data(self, data, get_response=False):
        resp = None
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        retry_count = 0
        while 1:  # TODO kind of nasty...
            if retry_count > 3:
                break
            try:
                retry_count += 1
                self.s.connect((HOST, SOCKET_PORT))
                break
            except ConnectionRefusedError:
                print("Connection failed")
                sleep(.5)
        if retry_count > 3:
            return None
        self.s.sendall(data)
        if get_response:
            message = []
            while 1:
                data = self.s.recv(1024)
                if not data:
                    break
                message.append(data)
            resp = decode(message)
        self.s.shutdown(socket.SHUT_RDWR)
        self.s.close()
        return resp

    def send_msg(self, msg: BaseMessage, get_response=False):
        data = encode(msg)
        return self._send_data(data, get_response)


if __name__ == '__main__':
    sb = RpiInterface()
    msg_to_send = SendMouseMovement(10, 10)
    sb.send_msg(msg_to_send)
