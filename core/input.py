from time import sleep
from typing import Optional, Callable, List, Tuple, TYPE_CHECKING
from queue import Queue
import pynput

from common.constants import INPUT_HANDLER_REFERENCE
from common.thread import ProducerThread
from core.rpi_interface import RpiInterface
from core.macros import Matcher

if TYPE_CHECKING:
    from core.core_state_manager import CoreStateManager


class InputHandlerThread(ProducerThread):
    def __init__(self, sm: 'CoreStateManager', *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sm: 'CoreStateManager' = sm
        self.g_x, self.g_y = 0, 0
        self.count = 0
        self.rpi = RpiInterface()
        self.key_listener = pynput.keyboard.Listener(on_release=self.on_release, on_press=self.on_press)
        self.mouse_listener = pynput.mouse.Listener(on_move=self.on_move, on_click=self.on_click)
        self.current_keys = set()
        self.forwarding = False
        self.matcher = Matcher(sm)
        self.override_key_listener_func: Optional[Callable] = None
        self.registered_key_listeners: List[Callable] = [
            self.matcher.add_key
        ]
        self.registered_mouse_listeners: List[Callable] = []
        self.key_queue = Queue()

        self.sm.register(INPUT_HANDLER_REFERENCE, self)

    def run(self):
        self.key_listener.start()
        self.mouse_listener.start()
        while self.sm.running:
            while not self.key_queue.empty():
                new_key = self.key_queue.get()
                if not self.override_key_listener_func:
                    for c in self.registered_key_listeners:
                        c(new_key=new_key)
                else:
                    self.override_key_listener_func(new_key=new_key)
            else:
                self.matcher.add_key(None)  # basically a heartbeat...
            sleep(.2)
        self.key_listener.stop()
        self.mouse_listener.stop()

    def register_macro(self, macro_tuple: Tuple[List, Optional[Callable]]):
        self.matcher.register_macro(macro_tuple)

    def register_key_listener(self, func: Callable):
        self.registered_key_listeners.append(func)

    def register_mouse_listener(self, func: Callable):
        self.registered_mouse_listeners.append(func)

    def override_key_listener(self, func: Callable):
        self.sm.print_status_message('Overriding default listeners')
        self.override_key_listener_func = func

    def clear_override_key_listener(self):
        self.sm.print_status_message('Clearing override for default listeners')
        self.override_key_listener_func = None

    def macro_status(self):
        if self.matcher.is_matching():
            return 0
        elif self.matcher.failed_match():
            return -1
        else:
            return 1

    def on_move(self, x, y):
        # Dont ask why these are flipped from what you would think
        delta_x = x - self.g_x
        delta_y = y - self.g_y
        self.g_x = x
        self.g_y = y

        print(f"Mouse Movement: {delta_x}, {delta_y}")
        for c in self.registered_mouse_listeners:
            c(event_type="move", d_x=delta_x, d_y=delta_y)

    def on_click(self, x, y, button, pressed):
        print(f"Mouse Click: {button}")
        for c in self.registered_mouse_listeners:
            c(event_type="click", button=button)

    def on_press(self, key):
        print("Key pressed: " + str(key))
        self.key_queue.put(key)

    def on_release(self, key):
        # self.key_queue.put((key, False))
        pass
