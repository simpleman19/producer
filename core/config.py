import os
from common.config import Config
from core.data import RemoteComputer

basedir = os.path.abspath(os.path.dirname(__file__))


class DevelopmentConfig(Config):
    FULL_SCREEN = True  # TODO need to figure out config inheritance
    DEBUG = True
    XBOXES = {
        '1': RemoteComputer(
            name="xbox1",
            control_ip="192.168.10.107",
            control_port=23,
            control_connection_type="telnet",
            rtsp_ip="192.168.10.30",
            rtsp_port=8554,
            control_video_endpoint='/0',
            ts_video_endpoint='/0.ts',
            record_vid_endpoint='/0',
            audio_separate=False,
        ),
        '2': RemoteComputer(
            name="xbox2",
            control_ip="192.168.10.106",
            control_port=23,
            control_connection_type="telnet",
            rtsp_ip="192.168.10.30",
            rtsp_port=8554,
            control_video_endpoint='/4',
            ts_video_endpoint='/4.ts',
            record_vid_endpoint='/4',
            audio_separate=False,
        ),
        '3': RemoteComputer(
            name="xbox3",
            control_ip="192.168.10.109",
            control_port=23,
            control_connection_type="telnet",
            rtsp_ip="192.168.10.30",
            rtsp_port=8554,
            control_video_endpoint='/8',
            ts_video_endpoint='/8.ts',
            record_vid_endpoint='/8',
            audio_separate=False,
        ),
        '4': RemoteComputer(
            name="xbox4",
            control_ip="192.168.10.108",
            control_port=23,
            control_connection_type="telnet",
            rtsp_ip="192.168.10.30",
            rtsp_port=8554,
            control_video_endpoint='/12',
            ts_video_endpoint='/12.ts',
            record_vid_endpoint='/12',
            audio_separate=False,
        ),
        '5': RemoteComputer(
            name="xbox5",
            control_ip="192.168.10.112",
            control_port=23,
            control_connection_type="telnet",
            rtsp_ip="192.168.10.31",
            rtsp_port=8554,
            control_video_endpoint='/0',
            ts_video_endpoint='/0.ts',
            record_vid_endpoint='/0',
            audio_separate=False,
        ),
        '6': RemoteComputer(
            name="xbox6",
            control_ip="192.168.10.115",
            control_port=23,
            control_connection_type="telnet",
            rtsp_ip="192.168.10.31",
            rtsp_port=8554,
            control_video_endpoint='/4',
            ts_video_endpoint='/4.ts',
            record_vid_endpoint='/4',
            audio_separate=False,
        ),
        '7': RemoteComputer(
            name="xbox7",
            control_ip="192.168.10.123",
            control_port=23,
            control_connection_type="telnet",
            rtsp_ip="192.168.10.31",
            rtsp_port=8554,
            control_video_endpoint='/8',
            ts_video_endpoint='/8.ts',
            record_vid_endpoint='/8',
            audio_separate=False,
        ),
        '8': RemoteComputer(
            name="xbox8",
            control_ip="192.168.10.122",
            control_port=23,
            control_connection_type="telnet",
            rtsp_ip="192.168.10.31",
            rtsp_port=8554,
            control_video_endpoint='/12',
            ts_video_endpoint='/12.ts',
            record_vid_endpoint='/12',
            audio_separate=False,
        ),
    }
    COMPUTERS = {
        '1': RemoteComputer(
            name="gaming1",
            control_ip="192.168.10.147",
            control_port=50007,
            control_connection_type="socket",
            rtsp_ip="192.168.10.35",
            rtsp_port=8554,
            control_video_endpoint='/0',
            audio_separate=False,
        ),
        '2': RemoteComputer(
            name="gaming2",
            control_ip="192.168.10.153",
            control_port=50007,
            control_connection_type="socket",
            rtsp_ip="192.168.10.35",
            rtsp_port=8554,
            control_video_endpoint='/4',
            audio_separate=False,
        ),
        '3': RemoteComputer(
            name="gaming3",
            control_ip="192.168.10.154",
            control_port=50007,
            control_connection_type="socket",
            rtsp_ip="192.168.10.35",
            rtsp_port=8554,
            control_video_endpoint='/4',
            audio_separate=False,
        ),
        '4': RemoteComputer(
            name="gaming4",
            control_ip="192.168.10.155",
            control_port=50007,
            control_connection_type="socket",
            rtsp_ip="192.168.10.35",
            rtsp_port=8554,
            control_video_endpoint='/4',
            audio_separate=False,
        ),
        '5': RemoteComputer(
            name="gaming5",
            control_ip="192.168.10.157",
            control_port=50007,
            control_connection_type="socket",
            rtsp_ip="192.168.10.35",
            rtsp_port=8554,
            control_video_endpoint='/4',
            audio_separate=False,
        ),
        '6': RemoteComputer(
            name="gaming6",
            control_ip="192.168.10.158",
            control_port=50007,
            control_connection_type="socket",
            rtsp_ip="192.168.10.35",
            rtsp_port=8554,
            control_video_endpoint='/4',
            audio_separate=False,
        ),
    }
    RECORDING_SERVER = RemoteComputer(
        name="recording",
        control_ip="192.168.10.199",
        control_port=50007,
        control_connection_type="socket",
    )


class ProductionConfig(Config):
    pass


class TestingConfig(ProductionConfig):
    pass


def load_config(config_name=None) -> Config:
    if not config_name:
        config_name = os.environ.get('APP_CONFIG', 'development')
    print("Starting up in: " + config_name)
    return config[config_name]()


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig
}
