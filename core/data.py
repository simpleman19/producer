from enum import Enum


class RemoteComputerType(Enum):
    PI_AND_ARDUINO = "Pi + Arduino over i2c"
    URAY_AND_ARDUINO = "UrayEncoder + Arduino over ethernet"


class RemoteComputer(object):
    def __init__(self, control_ip=None, control_port=None, name=None, rtsp_port=None, rtsp_ip=None,
                 control_video_endpoint=None, record_vid_endpoint=None, ts_video_endpoint=None,
                 audio_separate=False, audio_endpoint=None, control_connection_type="telnet"):
        self.control_ip = control_ip
        self.control_port = control_port
        self.control_connection_type = control_connection_type
        self.name = name
        self.rtsp_port = rtsp_port
        self.rtsp_ip = rtsp_ip
        self.control_video_endpoint = control_video_endpoint
        self.record_vid_endpoint = record_vid_endpoint
        self.ts_video_endpoint = ts_video_endpoint
        self.audio_separate = audio_separate
        self.audio_endpoint = audio_endpoint
