import queue

from typing import TYPE_CHECKING, Callable, Optional

from common.state_manager import StateManager
from common.constants import SOCKET_MANAGER_REFERENCE
import datetime

if TYPE_CHECKING:
    from common.socket_messages import BaseMessage
    from core.data import RemoteComputer
    from core.socket_manager import SocketManager


class CoreStateManager(StateManager):
    _running = True

    def __init__(self):
        super().__init__()
        self.registered_objs = {}
        self._socket_message_queue = queue.Queue()
        self._status_message_queue = queue.Queue()

    @property
    def socket_manager(self) -> 'SocketManager':
        return self.get_obj(SOCKET_MANAGER_REFERENCE)

    def send_socket_message(self, computer: 'RemoteComputer', message: 'BaseMessage',
                            callback: Optional[Callable] = None):
        self._socket_message_queue.put((computer, message, callback))

    def print_status_message(self, message: str):
        date = datetime.datetime.now().strftime("%m-%d %H:%M:%S")
        self._status_message_queue.put(f"{date}: {message}\n")
