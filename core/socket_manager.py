import socket
from typing import List, Tuple, Dict, Optional, Callable, TYPE_CHECKING
from telnetlib import Telnet

from common.constants import SOCKET_PORT, SOCKET_MANAGER_REFERENCE
from common.errors import merry
from common.socket_messages import encode, encode_telnet, decode, BaseMessage
from time import sleep
from datetime import datetime, timedelta

from common.thread import ProducerThread

if TYPE_CHECKING:
    from core.core_state_manager import CoreStateManager
    from core.data import RemoteComputer
    from common.config import Config


class SocketManager(ProducerThread):

    def __init__(self, sm: 'CoreStateManager', config: 'Config', *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.state_manager: 'CoreStateManager' = sm
        self.config: 'Config' = config
        self.open_sockets: Dict[Tuple[str, str], Tuple[Optional[datetime], Optional[socket.socket]]] = {}
        self.open_telnets: Dict[Tuple[str, str], Tuple[Optional[datetime], Optional[Telnet]]] = {}
        self.message_handlers: Dict[str, List[Callable]] = {}
        self.state_manager.register(SOCKET_MANAGER_REFERENCE, self)

    def run(self):
        while self.state_manager.running:
            if not self.state_manager._socket_message_queue.empty():
                remote_computer: 'RemoteComputer'
                message: BaseMessage
                remote_computer, message, callback = self.state_manager._socket_message_queue.get()
                resp = self._send_message(computer=remote_computer, data=message, return_response=True)
                if resp:
                    if callback:
                        callback(resp)
                    else:
                        handlers = self.message_handlers.get(resp.type_string, None)
                        if handlers:
                            for h in handlers:
                                self.run_handler(h, resp)
            else:
                for k, v in self.open_telnets.items():
                    timestamp, _ = v
                    if timestamp and timestamp < datetime.now() - timedelta(seconds=120):
                        ip, port = k
                        self._close_telnet(ip, port)
                for k, v in self.open_sockets.items():
                    timestamp, _ = v
                    if timestamp and timestamp < datetime.now() - timedelta(seconds=120):
                        ip, port = k
                        self._close_socket(ip, port)
                sleep(.1)

    @merry._try
    def run_handler(self, h: Callable, message: BaseMessage):
        h(message=message)

    def _send_message(self, computer: 'RemoteComputer', data: 'BaseMessage', return_response=False) \
            -> Optional[BaseMessage]:
        if not data or not computer:
            print(f"ERROR, send message called with data: {data} and computer: {computer}")
            return None
        ip = str(computer.control_ip)
        port = str(computer.control_port)
        if computer.control_connection_type == "socket":
            timestamp, s = self.open_sockets.get((ip, port), (None, None))
            if not s:
                s = self._open_socket(ip, port)
            msg_encoded = encode(data)
            if msg_encoded and s:
                ret_val = self._send_data(s, msg_encoded, return_response)
            else:
                ret_val = None
                print(f"Failed to send message: {data.type_string}, {data.data}")
                self.state_manager.print_status_message(f"Failed to send message: {data.type_string}, {data.data}")
            self._close_socket(ip, port)
            return ret_val
        elif computer.control_connection_type == "telnet":
            t = self._open_telnet(ip, port)
            if t:
                self._send_telnet_data(t, data)
                self.open_telnets[(ip, port)] = (datetime.now(), t)
            else:
                print(f"Failed telnet connection to {ip}:{port}")

    def _send_telnet_data(self, t: Telnet, data: BaseMessage):
        msg = encode_telnet(data)
        t.write(msg)

    def _open_telnet(self, ip: str, port: str):
        timestamp, t = self.open_telnets.get((ip, port), (None, None))
        if not t:
            try:
                t = Telnet(ip, port)
            except OSError as e:
                print(f"Failed to connect to host: {ip}:{port}")
                return None
            ret_val = t.read_until(b"ready", 1)
            if ret_val != b"ready":
                print('Failed to connect to arduino')
                t.close()
                t = None
        self.open_telnets[(ip, port)] = (datetime.now(), t)
        return t

    def _close_telnet(self, ip, port):
        timestamp, t = self.open_telnets[(ip, port)]
        if t:
            t.close()
            self.open_telnets[(ip, port)] = (None, None)

    def _open_socket(self, ip: str, port: str):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        retry_count = 0
        while 1:  # TODO kind of nasty...
            if retry_count > 3:
                break
            try:
                retry_count += 1
                s.connect((ip, int(port)))
                break
            except ConnectionRefusedError:
                print("Connection failed")
                sleep(.5)
        if retry_count > 3:
            return None
        if s:
            self.open_sockets[(ip, port)] = (datetime.now(), s)
        return s

    def _close_socket(self, ip: str, port: str):
        timestamp, s = self.open_sockets.get((ip, port), (None, None))
        if s:
            s.shutdown(socket.SHUT_RDWR)
            s.close()
            self.open_sockets[(ip, port)] = (None, None)

    def register_message_handler(self, message: BaseMessage, callback: Callable):
        if not self.message_handlers.get(message.type_string):
            self.message_handlers[message.type_string] = [callback]
        else:
            self.message_handlers[message.type_string].append(callback)

    @staticmethod
    def _send_data(s: socket, data, return_response) -> Optional[BaseMessage]:
        resp = None
        s.sendall(data)
        if return_response:
            message = []
            while 1:
                data = s.recv(1024)
                if not data:
                    break
                message.append(data)
            resp = decode(message)
        return resp


if __name__ == '__main__':
    from core.core_state_manager import CoreStateManager
    from core.data import RemoteComputer
    from common.config import Config
    from common.socket_messages import SendMouseClick

    test_c = RemoteComputer(control_ip="10.0.0.187", control_port=23)
    sm = SocketManager(CoreStateManager(), Config())
    sm._send_message(test_c, SendMouseClick(button=3))
