from time import sleep
from typing import Dict

from pynput.keyboard import Key, KeyCode

from common.config import Config
from common.constants import MACHINE_MANAGER_REFERENCE, INPUT_HANDLER_REFERENCE
from common.thread import ProducerThread
from constants import PYNPUT_COMPUTER_KEY_MAP
from core.config_macros.cod import ROGUE_COMPANY_CONFIG, NEW_COD_4_V_4
from core.config_macros.overwatch import OVERWATCH
from core.config_macros.valorant import VALORANT
from core.core_state_manager import CoreStateManager
from core.input import InputHandlerThread
from core.machine_manager import MachineManagerThread
from core.macros import KeyMatch, Capture

PLAYER_1 = "1"
PLAYER_2 = "2"
PLAYER_3 = "6"
PLAYER_4 = "7"
PLAYER_5 = "f5"
PLAYER_6 = "f6"
PLAYER_7 = "f7"
PLAYER_8 = "f8"
PLAYER_9 = "f9"
PLAYER_10 = "f10"
PLAYER_11 = "f11"
PLAYER_12 = "f12"
DEFAULT = 'f1'


class NewXboxMacros(ProducerThread):
    def __init__(self, sm: CoreStateManager, config: Config, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sm = sm
        self.config = config

        self.xbox_configs: Dict[str, Dict] = NEW_COD_4_V_4

        self.computer_configs: Dict[str, Dict] = ROGUE_COMPANY_CONFIG

        while not self.sm.get_obj(MACHINE_MANAGER_REFERENCE):
            sleep(.5)
        self.mm: MachineManagerThread = self.sm.get_obj(MACHINE_MANAGER_REFERENCE)
        if not self.mm:
            print("Error locating machine manager object")

        while not self.sm.get_obj(INPUT_HANDLER_REFERENCE):
            sleep(.5)
        self.input_obj: InputHandlerThread = self.sm.get_obj(INPUT_HANDLER_REFERENCE)

        if not self.input_obj:
            print("Error locating input object, could not register macro")
        else:
            self.input_obj.register_macro(
                ([KeyMatch(Key.alt), KeyMatch(KeyCode(char='z')), Capture()], self.toggle_forwarding)
            )
            self.input_obj.register_macro(
                ([KeyMatch(Key.alt), KeyMatch(KeyCode(char='a')), Capture()], self.toggle_computer_forwarding)
            )
        self.current_config = None
        self.current_config_type = None

    def toggle_forwarding(self, config_number=None):
        if config_number:
            print("Attempting to start sending players to xboxes")
            if config_number in self.xbox_configs.keys():
                self.current_config_type = 'xboxes'
                self.current_config = self.xbox_configs.get(config_number)
                self.input_obj.override_key_listener(self.key_forwarder)
                config_name = self.current_config.get('name', "unamed...")
                msg = f"Loading in forwarding config: {config_number} - {config_name}"
            else:
                print("error, unknown config number")
                msg = f"Unknown config: {config_number}"
        else:
            msg = "Unknown error in new console macros forwarding handler"
        self.sm.print_status_message(msg)

    def toggle_computer_forwarding(self, config_number=None):
        if config_number:
            print("Attempting to start sending players to computers")
            if config_number in self.computer_configs.keys():
                self.current_config_type = 'computers'
                self.current_config = self.computer_configs.get(config_number)
                self.input_obj.override_key_listener(self.key_forwarder)
                config_name = self.current_config.get('name', "unamed...")
                msg = f"Loading in forwarding config: {config_number} - {config_name}"
            else:
                print("error, unknown config number")
                msg = f"Unknown config: {config_number}"
        else:
            msg = "Unknown error in new console macros forwarding handler"
        self.sm.print_status_message(msg)

    def key_forwarder(self, new_key: Key):
        player = new_key
        if isinstance(player, KeyCode) and player.char == 'x':
            self.input_obj.clear_override_key_listener()
            return
        if isinstance(player, Key):
            player_key = PYNPUT_COMPUTER_KEY_MAP.get(player, None)
        elif isinstance(player, KeyCode):
            player_key = player.char
        else:
            player_key = None
        if self.current_config and player_key:
            config_dict = self.current_config.get(player_key)
            if config_dict and isinstance(config_dict, dict):
                for p, xbox_tuple in config_dict.items():
                    xbox, key = xbox_tuple
                    if self.current_config_type == 'xboxes':
                        self.sm.print_status_message(f"Sending player: {key} to xbox: {xbox}")
                        self.mm.send_xbox_key(xbox, key)
                    if self.current_config_type == 'computers':
                        self.sm.print_status_message(f"Sending player: {key} to computer: {xbox}")
                        self.mm.send_computer_key(xbox, key)
            elif config_dict:
                computer_number, key = config_dict
                if self.current_config_type == 'xboxes':
                    self.sm.print_status_message(f"Sending player: {key} to xbox: {computer_number}")
                    self.mm.send_xbox_key(computer_number, key)
                if self.current_config_type == 'computers':
                    self.sm.print_status_message(f"Sending player: {key} to computer: {computer_number}")
                    self.mm.send_computer_key(computer_number, key)
            else:
                self.sm.print_status_message(f"Unknown player: {player}")
        else:
            self.sm.print_status_message(f"Unknown error in new xbox macros")

    def run(self):
        while self.sm.running:
            sleep(1)
