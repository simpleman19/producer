from typing import Dict


class StateManager(object):
    _running = True

    def __init__(self):
        self.registered_objs: Dict[str, object] = {}

    @property
    def running(self):
        return self._running

    @running.setter
    def running(self, running):
        self._running = running

    def to_dict(self):
        return {
            'running': self.running,
        }

    def register(self, reference, obj):
        self.registered_objs[reference] = obj

    def get_obj(self, reference):
        return self.registered_objs.get(reference, None)
