class Config(object):
    DEBUG = False
    TESTING = False

    def get(self, attr: str, default=None):
        return getattr(self, str(attr), default)
