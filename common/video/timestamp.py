from math import floor

def timestamp_from_seconds(seconds) -> str:
    hours = floor(seconds / 3600)
    remaining_secs = seconds % 3600
    minutes = floor(remaining_secs / 60)
    remaining_secs = floor(remaining_secs % 60)
    return f"{hours:02}:{minutes:02}:{remaining_secs:02}"


if __name__ == '__main__':
    print(timestamp_from_seconds(7281))
