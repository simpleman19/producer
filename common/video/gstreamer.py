import subprocess
import shutil
from time import sleep, time


# gst-launch-1.0 rtspsrc location=rtspt://192.168.10.31:8554/0 latency=500 buffer-mode=auto ! rtph264depay ! h264parse ! mpegtsmux ! filesink location=stream.ts
from typing import Optional


class Gstreamer(object):
    def __init__(self):
        self.p: Optional[subprocess.Popen] = None
        self.rtsp_url = None
        self.output_path = None
        self.time: float = 0

    def record_rtsp(self, rtsp_url: str = None, output_path: str = None):
        # pipeline = [
        #     'rtspsrc', f"location={rtsp_url}", 'latency=500', 'buffer-mode=auto',
        #     '!', 'rtph264depay',
        #     '!', 'h264parse',
        #     '!', 'mpegtsmux',
        #     '!', 'filesink', f"location={output_path}"
        # ]
        # pipeline = [
        #     'rtspsrc', f"location={rtsp_url}", 'latency=500', 'buffer-mode=auto', 'name=src',
        #     'src.', '!', 'rtph264depay', '!', 'h264parse', '!', 'mux.',
        #     'src.', '!', 'decodebin', '!', 'audioconvert', '!', 'voaacenc', '!', 'mux.',
        #     'mpegtsmux', 'name=mux', '!', 'filesink', f"location={output_path}"
        # ]
        pipeline = [
            'rtspsrc', f"location={rtsp_url}", 'latency=500', 'buffer-mode=auto', 'name=src',
            'src.', '!', 'rtph264depay', '!', 'h264parse', '!', 'mux.',
            'src.', '!', 'decodebin', '!', 'audioconvert', '!', 'avenc_aac', '!', 'mux.',
            'mpegtsmux', 'name=mux', '!', 'filesink', f"location={output_path}"
        ]
        self.rtsp_url = rtsp_url
        self.output_path = output_path
        self.start(pipeline)
        self.time = time()

    def start(self, pipeline):
        gstreamer = shutil.which("gst-launch-1.0")
        if not gstreamer:
            raise FileNotFoundError("Failed to find gst-launch-1.0")
        cmd = [gstreamer]
        for p in pipeline:
            cmd.append(p)
        retry_count = 3
        while retry_count > 0 and not self.p:
            try:
                self._run(cmd)
            except subprocess.CalledProcessError as e:
                retry_count -= 1
                sleep(2)

    def stop(self):
        self.p.kill()
        return self.p.wait()

    def _run(self, cmd):
        print(" ".join(cmd))
        self.p = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
        if self.p.poll():
            self.p = None

