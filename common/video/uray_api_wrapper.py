from xml.etree import ElementTree as ET
import requests

class UrayApiWrapper(object):
    def __init__(self, ip):
        self.ip = ip

    def get_status(self):
        r = requests.get(f"http://{self.ip}/get_status", auth=('admin', 'admin'))
        if r.content:
            root = ET.fromstring(r.content)
            for child in root.iter("*"):
                print(child.tag, child.attrib, child.text.strip())
        return {

        }


if __name__ == '__main__':
    ur = UrayApiWrapper(ip='192.168.10.31')
    print(ur.get_status())
