from common.state_manager import StateManager
from common.thread import ProducerThread
from common.video.ffmpeg import Ffmpeg


class StreamThread(ProducerThread):
    def __init__(self, sm: StateManager, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sm = sm
        self.ffmpeg = Ffmpeg()
        self.ffmpeg.set_video_input('/dev/video0')
        self.ffmpeg.set_destination_uri("rtsp://localhost:8550/live")

    def running(self):
        return self.sm.running

    def run(self):
        self.ffmpeg.start_stream(self.running)
