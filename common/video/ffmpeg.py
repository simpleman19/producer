import subprocess
import shutil
import select
from typing import Optional
from time import sleep, time


class Ffmpeg(object):
    def __init__(self):
        self.ffmpeg = shutil.which("ffmpeg")
        if not self.ffmpeg:
            raise FileNotFoundError("Failed to find ffmpeg")
        self.v_input = None
        self.input_format = "v4l2"
        self.v_codec = "h264_omx"
        self.v_size = "1920x1080"
        self.v_bitrate = "8M"
        self.v_framerate = "30"
        self.destination_uri = None
        self.p = None

    def create_split_from_ts_file(self, source_file_path, start_timestamp, length_timestamp, output_file_path):
        cmd = [self.ffmpeg, '-y', '-i', source_file_path, '-c', 'copy', '-ss', start_timestamp, '-t', length_timestamp,
               '-map', '0:v', '-map', '0:a', output_file_path]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
        p.wait()
        return p.returncode

    def set_video_input(self, device):
        devices, _ = get_all_devices()
        if device in devices:
            self.v_input = device
        else:
            raise IOError("Video input not found")

    def set_destination_uri(self, uri):
        self.destination_uri = uri

    def start_stream(self, run_callback):

        cmd = [self.ffmpeg, '-y', '-f', self.input_format, '-i', self.v_input, '-tune', 'zerolatency', '-codec:v',
               self.v_codec, '-r', str(self.v_framerate), '-s', self.v_size, '-b:v', self.v_bitrate, '-f', 'rtsp',
               '-rtsp_transport', 'tcp', '-muxdelay', '0.1', self.destination_uri]
        retry_count = 3
        while retry_count > 0 and run_callback():
            try:
                self._run(cmd, run_callback)
            except subprocess.CalledProcessError as e:
                retry_count -= 1
                sleep(2)

    def _run(self, cmd, run_callback):
        print(" ".join(cmd))
        self.p = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
        poll_obj = select.poll()
        poll_obj.register(self.p.stdout, select.POLLIN)
        delims = ["\n", "\r"]
        count = 0
        while run_callback():
            count = (count + 1) % 100
            if count == 0 and not check_stream(self.destination_uri):
                break
            poll_result = poll_obj.poll(0)
            if poll_result:
                ch = self.p.stdout.read(1)
                if ch in delims:
                    print()
                elif ch:
                    print(ch, end="")
                else:
                    break
            else:
                if self.p.poll():
                    break
                sleep(.1)
        self.p.stdout.close()
        if not run_callback() or not not check_stream(self.destination_uri):
            self.p.kill()
        return_code = self.p.wait()
        if return_code:
            raise subprocess.CalledProcessError(return_code, cmd)


def get_all_devices():
    v4_l2_ctl = shutil.which("v4l2-ctl")
    if not v4_l2_ctl:
        raise FileNotFoundError("Failed to find v4l2-ctl")

    cmd = [v4_l2_ctl, '--list-devices']

    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = proc.communicate()
    devices = []
    for line in out.decode("utf-8").splitlines():
        if "/dev" in line:
            devices.append(line.strip())
    return devices, out.decode("utf-8")


def check_stream(url):
    ffprobe = shutil.which("ffprobe")
    if not ffprobe:
        raise FileNotFoundError("Failed to find ffprobe")

    cmd = [ffprobe, '-rtsp_transport', 'tcp', url]

    print("Checking stream")
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return proc.wait() == 0


class FfmpegTsRecorder(object):
    def __init__(self):
        self.p: Optional[subprocess.Popen] = None
        self.rtsp_url = None
        self.output_path = None
        self.time: float = 0

    def record_ts(self, ts_url: str = None, output_path: str = None):
        pipeline = [
            '-i', ts_url, "-c:v", "copy", "-c:a", "copy", output_path
        ]
        self.rtsp_url = ts_url
        self.output_path = output_path
        self.start(pipeline)
        self.time = time()

    def start(self, pipeline):
        ffmpeg = shutil.which("ffmpeg")
        if not ffmpeg:
            raise FileNotFoundError("Failed to find gst-launch-1.0")
        cmd = [ffmpeg]
        for p in pipeline:
            cmd.append(p)
        retry_count = 3
        while retry_count > 0 and not self.p:
            try:
                self._run(cmd)
            except subprocess.CalledProcessError as e:
                retry_count -= 1
                sleep(2)

    def stop(self):
        self.p.kill()
        return self.p.wait()

    def _run(self, cmd):
        print(" ".join(cmd))
        self.p = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
        if self.p.poll():
            self.p = None
