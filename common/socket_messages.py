import json
from common.constants import END_OF_MESSAGE, I2C_MOUSE_MOVEMENT, int_to_byte, I2C_MOUSE_CLICK
from typing import Optional


class BaseMessage(object):
    type_string = 'Invalid'

    def __init__(self):
        self.data = {}

    def to_json(self):
        try:
            return json.dumps({'type': self.type_string, 'data': self.data})
        except TypeError:
            print("Failed to turn message into json:")
            print(self.data)
            return None

    @staticmethod
    def from_json(data):
        json_dict = json.loads(data)
        type_string = json_dict.get('type')
        data = json_dict.get('data', None)

        msg_obj = BaseMessage()
        msg_obj.type_string = type_string
        msg_obj.data = data
        return msg_obj


class SendString(BaseMessage):
    type_string = 'SendString'

    def __init__(self, str_val=None):
        super().__init__()
        self.data['string'] = str_val


class SendMouseMovement(BaseMessage):
    type_string = 'SendMouseMovement'

    def __init__(self, x=None, y=None):
        super().__init__()
        self.data['x'] = x
        self.data['y'] = y


class SendMouseClick(BaseMessage):
    type_string = 'SendMouseClick'

    def __init__(self, button=None):
        super().__init__()
        self.data['button'] = button

    def to_json(self):
        data = self.data.copy()
        data['button'] = str(self.data['button'].value)
        try:
            return json.dumps({'type': self.type_string, 'data': data})
        except TypeError:
            print("Failed to turn message into json:")
            print(self.data)
            return None


class RequestStatus(BaseMessage):
    type_string = 'RequestStatus'

    def __init__(self):
        super().__init__()


class ShutdownSB(BaseMessage):
    type_string = 'ShutdownSB'

    def __init__(self):
        super().__init__()


class ServerStatus(BaseMessage):
    type_string = 'ServerStatus'

    def __init__(self, state_manager_dict=None):
        super().__init__()
        self.data['state'] = state_manager_dict


class RecorderManagement(BaseMessage):
    type_string = 'RecorderManagement'

    def __init__(self, url=None, action=None, output_name=None):
        super().__init__()
        self.data['url'] = url
        self.data['action'] = action
        self.data['output_name'] = output_name


class GetRecordingStatus(BaseMessage):
    type_string = 'GetRecordingStatus'

    def __init__(self):
        super().__init__()


class RecordingStatus(BaseMessage):
    type_string = 'RecordingStatus'

    def __init__(self, recording_status=None):
        super().__init__()
        self.data['status'] = recording_status


class RecorderCreateReplayFile(BaseMessage):
    type_string = 'RecorderCreateReplayFile'

    def __init__(self, url=None, length=None, xbox_name=None):
        super().__init__()
        self.data['url'] = url
        self.data['length'] = length
        self.data['xbox_name'] = xbox_name


def encode(message: BaseMessage) -> Optional[bytes]:
    json_data = message.to_json()
    if json_data:
        return json_data.encode("utf-8") + END_OF_MESSAGE
    return None


def encode_telnet(msg: BaseMessage) -> bytes:
    if msg.type_string == 'SendString':
        string = msg.data.get('string', None)
        if string:
            return string + bytes([0xAA])
    elif msg.type_string == 'SendMouseMovement':
        x = msg.data.get('x', 0)
        y = msg.data.get('y', 0)
        if x or y:
            return bytes([I2C_MOUSE_MOVEMENT, int_to_byte(int(x)), int_to_byte(int(y)), 0xAA])
    elif msg.type_string == 'SendMouseClick':
        button = msg.data.get('button', None)
        if button:
            return bytes([I2C_MOUSE_CLICK, int_to_byte(int(button.value)), 0xAA])

    return b"error"


def decode(message) -> Optional[BaseMessage]:
    if isinstance(message, list):
        message = b''.join(message)
    message = message.replace(END_OF_MESSAGE, b'')
    if message == b'':
        return None
    return BaseMessage.from_json(message.decode("utf-8"))
