import subprocess
import shutil
from time import sleep
import os

from common.thread import ProducerThread

'''
usage: rtsp-simple-server [<flags>]

rtsp-simple-server v0.6.1

RTSP server.

Flags:
  --help                 Show context-sensitive help (also try --help-long and --help-man).
  --version              print version
  --protocols="udp,tcp"  supported protocols
  --rtsp-port=8554       port of the RTSP TCP listener
  --rtp-port=8000        port of the RTP UDP listener
  --rtcp-port=8001       port of the RTCP UDP listener
  --read-timeout=5s      timeout for read operations
  --write-timeout=5s     timeout for write operations
  --publish-user=""      optional username required to publish
  --publish-pass=""      optional password required to publish
  --read-user=""         optional username required to read
  --read-pass=""         optional password required to read
  --pre-script=""        optional script to run on client connect
  --post-script=""       optional script to run on client disconnect

'''


class RtspServerThread(ProducerThread):
    def __init__(self, rtsp_port, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.protos = "udp,tcp"
        self.rtsp_port = rtsp_port

    def run(self):
        SIMPLE_RTSP = shutil.which("rtsp-simple-server")
        if not SIMPLE_RTSP:
            raise FileNotFoundError("Failed to find rtsp-simple-server")
        cmd = [SIMPLE_RTSP, "--protocols", self.protos, '--rtsp-port', str(self.rtsp_port)]
        popen = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
        for stdout_line in iter(popen.stdout.readline, ""):
            print(stdout_line, end="")
        popen.stdout.close()
        return_code = popen.wait()
        if return_code:
            raise subprocess.CalledProcessError(return_code, cmd)
