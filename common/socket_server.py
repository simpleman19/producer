from threading import Thread

from common.config import Config
from common.socket_messages import BaseMessage, ServerStatus, decode, encode
import socket

from common.thread import ProducerThread
from common.state_manager import StateManager
from typing import Optional, Callable, Dict
from common.constants import END_OF_MESSAGE, SOCKET_PORT, I2C_MOUSE_MOVEMENT, int_to_byte, I2C_MOUSE_CLICK, \
    SOCKET_SERVER_REFERENCE
from time import sleep

from common.worker import Pool

HOST = '0.0.0.0'


class SocketServerThread(ProducerThread):
    def __init__(self, sm: StateManager, config, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.state_manager = sm
        self.config = config
        self.handlers: Dict[str, Callable] = {}
        self.add_default_handlers()
        self.state_manager.register(SOCKET_SERVER_REFERENCE, self)
        self.pool = Pool(4, False)

    def run(self):
        print("Running socket server on port: " + str(SOCKET_PORT))
        print("Initializing message thread pool")
        self.pool.run(False)
        print("Message thread pool running")
        while 1:
            s = None
            try:
                if not self.state_manager.running:
                    break
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.bind((HOST, SOCKET_PORT))
                break
            except OSError:
                print("Retrying to start socket server")
                sleep(2)
        if self.state_manager.running:
            s.listen(self.config.SOCKET_COUNT)
            s.settimeout(5)
            print("Server is running....")

        while self.state_manager.running:
            try:
                conn, addr = s.accept()
                print('Connected by', addr)
                self.pool.enqueue(self.handle_connection, conn=conn)
            except socket.timeout:
                pass
            except Exception:
                raise

        print("Server is dead...")

    def handle_connection(self, conn):
        try:
            message = []
            while 1:
                data = conn.recv(1024)
                if not data:
                    break
                message.append(data)
                if END_OF_MESSAGE in b''.join(message):
                    break
            msg = decode(message)
            resp = self.handle_message(msg)

            if resp is not None:
                print('Sending response')
                conn.sendall(encode(resp))
        except socket.timeout:
            pass
        except Exception:
            raise
        finally:
            if conn:
                print('Closing connection')
                conn.shutdown(socket.SHUT_RDWR)
                conn.close()

    def handle_message(self, msg: BaseMessage) -> Optional[BaseMessage]:
        if msg:
            handler = self.handlers[msg.type_string]
            if handler:
                return handler(msg=msg, sm=self.state_manager, config=self.config)
            else:
                print(msg)
        return None

    def add_message_handler(self, type_string: str, handler: Callable):
        self.handlers[type_string] = handler

    def add_default_handlers(self):
        self.handlers['RequestStatus'] = handle_request_status
        self.handlers['ShutdownSB'] = handle_shutdown
        # self.handlers['SendString'] = handle_send_string
        # self.handlers['SendMouseMovement'] = handle_send_mouse_movement
        # self.handlers['SendMouseClick'] = handle_send_mouse_click


def handle_request_status(sm: StateManager):
    return ServerStatus(sm.to_dict())


def handle_shutdown(sm: StateManager):
    sm.running = False

# TODO these should be somewhere else
# from rpi.i2c import write_str_to_i2c, write_block_data_to_i2c
#
# def handle_send_string(msg: BaseMessage):
#     print("Handling string: " + msg.data['string'])
#     write_str_to_i2c(msg.data['string'])
#
#
# def handle_send_mouse_movement(msg: BaseMessage):
#     print("Handling Mouse Movement x: " + str(msg.data['x']) + " y: " + str(msg.data['y']))
#     write_block_data_to_i2c(
#         [I2C_MOUSE_MOVEMENT, int_to_byte(int(msg.data['x'])), int_to_byte(int(msg.data['y']))])
#
#
# def handle_send_mouse_click(msg: BaseMessage):
#     print("Handling Mouse Click: " + str(msg.data['button']))
#     write_block_data_to_i2c([I2C_MOUSE_CLICK, int_to_byte(int(msg.data['button']))])
