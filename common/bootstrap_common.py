import threading
from time import sleep
from typing import List, Tuple, Optional, Dict, Type
import signal
from threading import Thread

from common.config import Config
from common.state_manager import StateManager

from common.thread import ProducerThread


class BootstrapApplication(ProducerThread):

    def __init__(self, thread_classes: List[Tuple[Type[ProducerThread], Optional[Dict]]], sm: StateManager,
                 config: Config, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.thread_classes = thread_classes
        self.state_manager = sm
        self.threads: Dict[str, Tuple[Type[ProducerThread], Dict, Thread]] = {}
        self.config = config
        self.sig_handlers_initialized = False

    def startup(self):
        for thread_class, func_args in self.thread_classes:
            t = self.start_thread(thread_class, func_args)
            self.threads[t.name] = (thread_class, func_args, t)

    def start_thread(self, thread_class, func_args):
        if not func_args:
            func_args = {}
        _kwargs = {
            'sm': self.state_manager,
            'config': self.config,
            **func_args
        }
        thread = thread_class(**_kwargs)
        t = Thread(target=thread.run)
        t.daemon = True
        t.name = str(thread)
        t.start()
        return t

    def run(self):
        self.init_sig_handlers()
        self.startup()
        while self.state_manager.running:
            new_threads = {}
            old_threads = []
            threads = self.threads
            for k, v in threads.items():
                thread_class, func_args, t = v
                if not t.is_alive() and self.state_manager.running:
                    print(f"Thread: {t.name} died")
                    t.join()
                    sleep(2)
                    print(f"Restarting Thread: {t.name}")
                    t = self.start_thread(thread_class, func_args)
                    new_threads[t.name] = (thread_class, func_args, t)
                    old_threads.append(k)
            sleep(2)
            for name, t in new_threads.items():
                self.threads[name] = t
            for k in old_threads:
                del self.threads[k]
        threads = self.threads
        for k, v in threads.items():
            _, _, t = v
            print(f"Waiting for: {t.name}")
            t.join()

    def handler(self, signum, frame):
        print(f"Handling: {signum} by shutting down")
        self.state_manager.running = False

    def init_sig_handlers(self):
        if not threading.current_thread() is threading.main_thread():
            self.sig_handlers_initialized = True
            print("Not main thread so skipping sig handlers")
        if not self.sig_handlers_initialized:
            print("Initializing sig handlers")
            signals = [
                signal.SIGINT,
                signal.SIGTERM,
            ]
            for s in signals:
                signal.signal(s, self.handler)
            self.sig_handlers_initialized = True
