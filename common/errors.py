from merry import Merry

merry = Merry()


@merry._except(Exception)
def catch_all(e):
    print('Unexpected error: ' + str(e))
