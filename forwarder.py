import pynput

from common.constants import SOCKET_MOUSE_CLICK_LEFT
from common.socket_messages import SendMouseMovement, SendMouseClick
from core.rpi_interface import RpiInterface

g_x, g_y = 0, 0
count = 0


def on_move(x, y):
    global g_x, g_y, count
    rpi = RpiInterface()
    # Dont ask why these are flipped from what you would think
    delta_x = x - g_x
    delta_y = y - g_y
    g_x = x
    g_y = y

    print(f"mouse moved: {delta_x}, {delta_y}")

    rpi = RpiInterface()
    msg_to_send = SendMouseMovement(delta_x, delta_y)
    rpi.send_msg(msg_to_send)


def on_click(x, y, button, pressed):
    print('{0} at {1}'.format(
        'Pressed' if pressed else 'Released',
        (x, y)))

    if not pressed:
        rpi = RpiInterface()
        msg = SendMouseClick(SOCKET_MOUSE_CLICK_LEFT)
        rpi.send_msg(msg)


def on_release():
    pass


if __name__ == "__main__":
    with pynput.mouse.Listener(on_move=on_move, on_click=on_click) as listener:
        listener.join()
    with pynput.keyboard.Listener(on_release=on_release) as listener:
        listener.join()
